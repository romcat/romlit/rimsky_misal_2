MODLITBA DNA

Dobrotivý Bože,

svätého Timoteja a Tita si ozdobil

čnost'ami apoštolov; *

prosíme t'a, pomáhaj nám na ich orodovanie, —
aby sme nábožne a sväto žili na tomto svete,

a tak si zaslúžili dôjst' do nebeskej Vlasti.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

27. januára
SV. ANGELY MERICI, PANNY

Spoločná omša panien (str. 746 n.)
alebo svätých vychovávatelov (str. 764 n.).

MODLITBA DNA

Milosrdný Bože, *

nech nás svätá panna Angela ustavične odporúča
tvojej otcovskej dobrote, —

aby sme nasledovaním jej lásky a múdrosti
ochotne prijali tvoje učenie

a vyznávali ho čnostným životom.

Skrze nášho Pána Ježiša Krista, tvojho Syna...

28. januára

SV. TOMÁŠA AKVINSKÉHO,
KNAZA A UČITEĽA CIRKVI

Spomienka

Spoločná omša učiteľov Cirkvi (str. 743 n.)
alebo duchovných pastierov (str. 731 n.).

492 VLASTNÉ OMŠE svÁTýCH

