MODLITBA DNA

Dobrotivý Bože,

u teba hriešnici nachádzajú milosrdenstvo

a svätí blaženosť; *

daj podiel s tvojimi svätými

aj svojmu služobníkovi M., (svojej služobníci M.,)
ktorému (ktorej) dnes (pri pohrebe)

preukaZujeme službu lásky, —

aby zbavený(á) pút smrti mohol (mohla)

v deň vzkriesenia stáť pred tvojou tvárou.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

lVĺilosrdný Bože, *

prinášame ti obetu zmierenia

v deň pohrebu tvojho služobníka M.
(tvojej služobnice M.)

a prosíme, —

ak ho (ju) ešte ťažia hriechy

a následky ľudských slabosti,

láskavo ho (ju) očistí a prijmi k sebe.
Skrze Krista, nášho Pána.

Pieseň vďaky o zosnulých (str. 399 n.).

SPEV NA PRIJÍMANIE (Flp 3. 20-21)

Očakávame Spasiteľa,

Pána Ježiša Krista.

On mocou, ktorou si môže podmaniť všetko,
pretvorí naše úbohé telo,

aby sa stalo podobným jeho oslávenému telu.

POHREBNÉ OMŠE 959

