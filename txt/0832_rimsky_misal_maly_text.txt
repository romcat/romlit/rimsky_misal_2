PO PRIJÍMANÍ

Milosrdný Bože, *

nech nás prijatá sviatosť pripraví na večné radosti, —
ktoré si svätý M. zaslúžil svojou vernou službou.
O to ťa prosíme skrze Krista, nášho Pána.

Alebo:

Všemohúci Bože,

posilnil si nás svätým pokrmom; *

pomôž nám nasledovať príklad svätého M., —
aby sme ťa uctievali s oddaným srdcom

a blížnemu slúžili s neúnavnou láskou.

Skrze Krista, nášho Pána.

6.0mša o duchovných pastieroch

ÚVODNY SPEV (Porov. ]er 3, 15)

Dám vám pastierov podl'a môjho srdca
a budú vás múdro spravovať a poučovať.

Alebo: (Dan 3, 84. 87)

Dobrorečte Pánovi, všetci jeho kňazi,
svätí a pokorní srdcom, chvál'te Pána.

MODLITBA DNA

Večný Bože, ty si obdaril svätého M. a M.
(svätých biskupov M. a M.)

duchom pravdy a lásky,

aby boli dobrými pastiermi tvojho ľudu; *

s úctou slávime ich sviatok a prosíme, —
aby nám ich príklad a orodovanie

pomáhali napredovať v kresťanskom živote.
Skrze nášho Pána Ježiša Krista, tvojho Syna...

732 SPOLOČNÉ OMŠE

