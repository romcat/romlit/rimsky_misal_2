NAD OBETN YMI DARMI

Milosrdný Bože,

pristupujeme k oltáru,

na ktorom je pripravený chlieb a víno; *
otvor nám dvere svojho večeradla, —
aby sme mohli sláviť nebeskú hostinu

a tešiť sa, že sme spoluobčanmi svätých
a členmi tvojej rodiny.

Skrze Krista, nášho Pána.

Osobitné spomienky v eucharistických modlitbách sú tie isté ako v pred-
chádzajúcom omšovom formulári.

SPEV NA PRIJÍMANIE (1 Jn ;, 2)

Už teraz sme Božími deťmi,
a ešte sa neukázalo, čím budeme.

PO PRIJÍMANÍ

Dobrotivý Otče, *

slávením Eucharistie sme Zvestovali

smrť a zmŕtvychvstanie tvojho Syna; -

daj, nech nám sila Oltárnej sviatosti pomáha,
aby sme toto veľké tajomstvo

vyznávali svojím životom.

Skrze Krista, nášho Pána.

4. PRI UDEĽOVANÍ
SVIATOSTI BIRMOVANIA

Keď sa udeľuje sviatosť birmovania cez omšu, prípadne bezprostredne
pred ňou alebo po nej, berie sa tento omšový formulár. Omšové rúcho
je červené alebo biele.

Tento formulár možno použiť vo všetky dni okrem nediel' adventných,
pôstnych a veľkonočných, okrem slávností, Popolcovej stredy a celého
Veľkého týždňa.

782 OMŠE PRI VYSLUHOVANÍ SVIATOSTÍ

