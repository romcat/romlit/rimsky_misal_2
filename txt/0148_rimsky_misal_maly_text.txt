NAD OBETNYMI DARMI

Všemohúci Bože,

nech ti je milá dnešná slávnostná obeta, *
ktorá nás dokonale Zmieruje s tebou —

a je najvyšším prejavom našej služby

na tvoju česť a slávu.

Skrze Krista, nášho Pána.

Pieseň vďaky Vianočná (str. 356 n.).
Ak sa recituje Rímsky kánon, použije sa vlastná vložka V „spoločenstve.

SPEV NA PRIJÍMANIE . (Ž 97, 3)

Všetky končiny zeme
uzreli spásu nášho Boha.

PO PRIJÍMANÍ

Prosíme ťa, dobrotivý Bože, *

nech nám Spasiteľ sveta, ktorý sa dnes narodil
a urobil nás tvojimi deťmi, —

dá aj účasť na večnom živote.

Skrze Krista, nášho Pána.

48 VIANOČNÉ OBDOBIE

