MODLITBA DNA

Všemohúci a večný Bože,

smieme t'a volať Otcom,

lebo si nás prijal za svoje deti; *

dopraj nám stále rásť v synovskej láske, —-

aby sme mohli dosiahnuť prisľúbené dedičstvo.
Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

V jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Láskavý Otče, *

prijmi dary plesajúcej Cirkvi,

ktorej si pripravil svätú veľkonočnú radosť, —
a priveď ju do večnej blaženosti.

Skrze Krista, nášho Pána.

Pieseň vďaky veľkonočné (str. 367 n.).

SPEV NA PRIJÍMANIE (Jn zo, 19)

Ježiš si stal do stredu a povedal učeníkom:
Pokoj vám. Aleluja.

PO PRIJÍMANÍ

Prosíme t'a, večný Bože, *

láskavo zhliadni na svoj ľud,

ktorý si duchovne obnovil
vel'konočnými sviatosťami, —

a priveď nás k slávnemu vzkrieseniu.
Skrze Krista, nášho Pána.

PONDELOK PO DRUHÉ] NEDELI 217

