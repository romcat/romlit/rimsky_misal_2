Alebo : (]n 5, 8)

Pán Ježiš hovorí: Môj Otec je práve tým oslávený,
že prinášate vel'a ovocia
a že sa stávate mojimi učeníkmi.

PO PRIJÍMANÍ

Nebeský Otče, v tejto eucharistickej obete

zahrnul si nás bohatstvom svojej milosti; *
prosíme ťa,

posilňuj sviatostným pokrmom všetkých,

ktorí sa z tvojej vôle venujú

budovaniu tohto sveta, —

aby boli odvážnymi svedkami evanjeliovej pravdy,
a tak umožňovali Cirkvi blahodarne vplývať

na stvárňovanie pozemského života.

Skrze Krista, nášho Pána.

13. ZA ]EDNOTU KRES'Í'ANOV

Tento omšový formulár možno použiť aj v nedele (( cez rok », ked' sa
konajú pobožností za zjednotenie kresťanov.

A
ÚVODNÉ? SPEV (]n 10, 14-15)

Pán Ježiš hovorí: ]a som dobrý pastier,
poznám svoje ovce a moje ovce poznajú mňa,
ako mňa pozná Otec, a ja poznám Otca.

Aj svoj život položím za ovce.

MODLITBA DNA

Všemohúci a večný Bože,
ty zjednocuješ rozdelených

868 OMŠE ZA ROZLIČNÉ POTREBY

