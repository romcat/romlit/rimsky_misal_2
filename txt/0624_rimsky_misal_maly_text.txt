30. apríla

SV. PIA V., PÁPEŽA

Spoločná omša duchovných pastierov-pápežov (str. 725 n.).

MODLITBA DNA

Dobrotivý Bože,

vo svojej prozretelnosti

si poslal svojmu l'udu svätého pápeža Pia,

aby hájil pravú vieru a obnovil liturgiu Cirkvi; *
daj, aby sme na jeho príhovor

slávili tajomstvá spásy so živou vierou —

a uvádzali ich do života činorodou láskou.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

524 VLASTNÉ OMŠE SVÁTYCH

