NAD OBETNYMI DARMI

Prosíme ťa, všemohúci Bože, *

milostivo zhliadni na našu kňaZSkú službu, —

aby sa naša obeta tebe páčila
a v nás rozmnožovala lásku.

Skrze Krista, nášho Pána.

Pieseň vďaky nedeľná (str. 374 n.).

SPEV NA PRIJÍMANIE

Pane, opora moja, útočište moje, osloboditeľ môj,
Bože môj, moja pomoc, tebe dôverujem.

Alebo :

Boh je láska.
Kto ostáva v láske, ostáva v Bohu
a Boh ostáva v ňom.

PO PRIJÍMANÍ

Dobrotivý Bože,

vrúcne t'a prosíme, *

aby nás liečivá moc tejto sviatosti
oslobodila od zlých žiadostí —

a pobádala k dobrému.

Skrze Krista, nášho Pána.

(Ž ĺ7: 3)

(1 Jn 4, 16)

 

]EDENÁSTA NEDEĽA

 

ÚVODNY SPEV

Čui, Pane, hlas môjho volania,
ty si moja pomoc, neodvrhui ma,
ani ma neopúšťai, Bože, moia spása.

Oslavná pieseň.

(Ž 26. 7-9)

]EDENÁSTA NEDEĽA 293

