i svätú a nepoškvrnenú obetu
tvojho veľkňaza Melchizedecha.

Sklonený so zopätými rukami pokračuje:

Pokorne ťa prosíme, všemohúci Bože,
prikáž svojmu svätému anjelovi

preniesť tieto dary

na tvoj nebeský oltár,

pred tvár tvojej božskej velebnosti,

aby nás všetkých,

ktorí máme účasť na tejto oltárnej obete
a prijmeme presväté telo '
a krv tvojho Syna,

vzpriami sa a prežehná sa; pritom hovorí:

naplnilo hojné nebeské požehnanie
a milosť.

Zopne ruky.

(Skrze nášho Pána Ježiša Krista. Amen.)

PRVÁ EUCHARISTICKÁ MODLITBA 415

