VEĽKONOČNÉ TROJDNIE

 

VEČERNÁ OMŠA NA PAMIATKU
PÁNOVE] VEČERE

 

Podl'a pradávnej cirkevnej tradície nie je dovolené v tento deň sláviť
svätú omšu bez účasti ludu.

Vo večerných hodinách, v čase, ktorý najlepšie vyhovuje veriacim,
slávi sa svätá omša na pamiatku Poslednej večere za účasti celej farnosti.
Všetci kňazi a prisluhujúci sa zúčastnia na slávení Eucharistie, každý
podľa hodnosti v duchovnej službe.

Kňazi, ktorí už slúžili svätú omšu pri svätení olejov alebo inú omšu
pre veriacich, môžu koncelebrovať večernú omšu.

Ak to vyžadujú pastoračné dôvody, miestny ordinár môže dovoliť
aj inú večernú omšu, nielen vo farskom kostole, ale aj v iných kostoloch
a verejných alebo poloverejných kaplnkách. Vo veľmi naliehavých prí-
padoch môže dovoliť svätú omšu aj v ranných hodinách pre veriacich,
ktorí sa nemôžu zúčastniť na večernej omši. Treba dbať, aby táto omša
nebola iba pre záujem jednotlivcov a na úkor hlavnej večernej omše.

Sväté prijímanie možno podávať iba cez omšu, ale chorým ho možno
zaniesť v ktorúkoľvek hodinu.

Úvodné obrady a Liturgia slova

1. Svätostánok má byť prázdny. V tejto omši sa má konsekrovať dosta-
točné množstvo hostií na prijímanie kňazov, diakonov a veriacich aj na
zajtrajší deň.

:. ÚVODNY SPEV (Porov. Gal 6, 14)
Hl'adajme slávu v kríži nášho Pána Ježiša Krista.

On je naša spása, náš život, naše vzkriesenie;
on nás oslobodil a spasil.

3. OSLAVNÁ PIESEN. Kým sa spieva oslavná pieseň, zvonia zvony.
Potom sa odmlčia až do Veľkonočnej vigílie, ak biskupská konferencia
alebo ordinár neuznali za vhodné ustanoviť ináč.

4. MODLITBA DNA

Nebeský Otče,

zišli sme sa osláviť pamiatku Poslednej večere, *
pri ktorej tvoj milovaný Syn,

prv než seba samého vydal na smrť,

ZELENY ŠTVRTOK 153

