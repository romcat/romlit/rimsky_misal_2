 

POPOLCOVÁ STREDA

 

V omši dnešného dňa sa požehnáva a rozdáva popol, pripravený
z olivových alebo iných ratolesti, ktoré boli požehnané na Kvetnú nedeľu
predchádzajúceho roku.

Úvodné obrady
a bohoslužba slova

ÚVODNY SPEV (Múd n, 24-25; 27)

Pane, zmílúvaš sa nad všetkými

a nepohr'daš ničím, čo si stvoril;

zatváraš oči nad hriechmi ľudí, aby sa kajali,
a si k nim láskavý, lebo si Boh a náš Pán.

Úkon kajúcnosti sa vynechá. Nahrádza ho značenie popolom.

MODLITBA DNA

Milosrdný Bože, *

dopraj nám týmto svätým pôstom

nastúpiť cestu duchovnej obnovy, ——

aby nás pokánie posilňovalo v boji

proti nepriateľom spásy.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

V jednote s Duchom Svätým po všetky veky vekov.

Požehnanie popola
a značenie popolom

Po homílii kňaz postojačky a so zopätýrni rukami hovorí:
Drahí bratia a sestry, (alebo iné vhodné oslovenie)
pokorne prosme Boha Otca,

aby hojnou milosťou požehnal tento popol,
ktorým budeme poznačení na znak kajúcnosti.

POPOLCOVÁ STREDA 75

