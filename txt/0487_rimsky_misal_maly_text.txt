: Pán s vami.

: I s duchom tvojím.

: Hore srdcia.

: Máme ich u Pána.

: Vzdávajme vďaky Pánovi, Bohu nášmu.
: Je to dôstojné a správne.

PŠPŠPŠ

je naozaj dôstojné a správne, dobré a spásonosné
vzdávať vďaky vždy a všade
tebe, Pane, svätý Otče, všemohúci a večný Bože,
a teba na slávnosť

(pri spomienke) (pri uctievaní)
svätého ]ozefa náležitými chválospevmi chváliť,
velebit' a oslavovať.
Lebo tohto spravodlivého muža
dal si panenskej Bohorodičke za ženícha,
a jeho, ako verného a múdreho služobníka,
ustanovil si za hlavu Svätej rodiny,
aby sa otcovsky staral o tvojho jednorodeného,
Duchom Svätým počatého Syna,
Ježiša Krista, nášho Pána.
Skrze neho tvoju velebu chvália anjeli,
klaňajú sa ti všetky nebeské zástupy,
i blažení serafini t'a oslavujú spoločným plesaním.
Prosíme, dovoľ i nám, aby sme t'a s nimi
v pokornej úcte chválili:

Svätý, svätý, svätý Pán Boh všetkých svetov...

O APOŠTOLOCH I

0 apoštoloch, pastieroch Božieho ľudu

Táto pieseň vďaky sa používa v omšiach o apoštoloch, najmä v omšiach
o svätom Petrovi a Pavlovi.

O APOŠTOLOCH I 387

13*

