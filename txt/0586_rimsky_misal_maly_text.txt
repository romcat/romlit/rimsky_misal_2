20. januára

SV. FABIÁNA, PÁPEŽA A MUČENÍKA

Spoločná omša mučeníkov (str. 713 n.)
alebo duchovných pastierov-pápežov (str. 725 n.).

MODLITBA DNA

Bože a Otče náš, ty si sláva Cirkvi

a sila jej duchovných pastierov; *

prosíme ťa, —

daj, aby sme na orodovanie svätého

mučeníka Fabiána

rástli vo viere, ktorú on hrdinsky vyznával,

a podľa jeho príkladu oddane ti slúžili až do smrti.
Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

SV. ŠEBASTIÁNA, MUČENÍKA

Spoločná omša mučeníkov (str. 713 n.).

MODLITBA DNA

Prosíme t'a, nekonečný Bože, *

udel' nám ducha sily, —

aby sme podl'a príkladu slávneho

mučeníka Šebastiána

poslúchali viac teba ako ľudí.

Skrze nášho Pána ]ežiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

V jednote s Duchom Svätým po všetky veky vekov.

486 VLASTNÉ OMŠE svÁTýCH

