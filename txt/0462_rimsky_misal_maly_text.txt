V.: Hore srdcia.

O.: Máme ich u Pána.

V.: Vzdávajme vďaky Pánovi, Bohu nášmu.
O.: Je to dôstojné a správne.

Je naozaj dôstojné a správne

spievať pieseň chvály a veleby tebe,

všemohúci Otče,

a pôstnym sebazapieraním uznávať tvoju dobrotu
a vzdávať ti vďaky.

Lebo ty chceš, aby sme premáhaním seba
krotili svoju pýchu

a otvárali srdcia pre chudobných.

Keď sa s nimi delíme o chlieb,
napodobňujeme tvoju dobrotu

i lásku tvojho Syna, nášho Pána Ježiša Krista.
Skrze neho klaňajú sa ti zástupy anjelov

a večne sa radujú v tvojej prítomnosti.
Prosíme t'a, pripoj k nim aj naše hlasy,

keď spoločne voláme na tvoju slávu:

Svätý, svätý, svätý Pán Boh všetkých svetov...

P ôSTN A IV

O duchovnom ovocí pôstu

Táto pieseň vďaky sa používa vo všedné dni pôstneho obdobia a v dni
prikázaného pôstu.

: Pán s vami.

: I s duchom tvojím.

: Hore srdcia.

: Máme ich 11 Pána.

: Vzdávajme vďaky Pánovi, Bohu nášmu.
: Je to dôstojné a správne.

PŠPŠPŠ

362 PIESNE VĎAKY

