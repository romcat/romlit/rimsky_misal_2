PO PRIJÍMANÍ

Dobrotivý Otče, vypočuj naše prosby, *
aby nás účasť

na svätých tajomstvách nášho vykúpenia
posilňovala v tomto živote —

a priviedla do večnej blaženosti.

Skrze Krista, nášho Pána.

 

ŠTVRTOK
po Tretej veľkonočnej nedeli

 

ÚVODNY SPEV (Ex 15, x-z)

Spievajme Pánovi,

lebo sa preslávil.

Pán je moja sila a moja udatnosť,
on ma zachránil. Aleluja.

MODLITBA DNA

Všemohúci a večný Bože,

v tomto veľkonočnom období

vrúcnejšie prežívame a lepšie poznávame

tvoju otcovskú dobrotu,

lebo si nás vyslobodil z temnoty bludov; *
prosíme t'a, pomáhaj nám, —

aby sme sa vždy verne pridržiavali tvojej pravdy.
Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

ŠTVRTOK PO TRETE] NEDELI 229

