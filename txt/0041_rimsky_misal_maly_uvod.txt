tári svätostánok s najsvätejšou Svia-
tost'ou, poklaknutim.

Ak sa v sprievode nesie kríž,
postaví sa vedľa oltára alebo na
iné vhodné miesto. Svietniky, ktoré
niesli posluhujúci, sa umiestnia
vedľa oltára alebo na stolík. Evan-
jeliár sa kladie na oltár.

85. Kňaz vystúpi k oltáru a ucti si
ho bozkom. Potom môže okiadzat'
oltár, pričom ho obíde dokola.

86. Keď to vykonal, ide k sedadlu.
Po úvodnom speve všetci stoja a
kňaz i veriaci sa prežehnajú. Kňaz
hovorí V mene Otca i Syna i Ducha
Svätého, Ľud odpovie Amen.

Potom kňaz obrátený k ľudu
rozopne ruky a pozdraví ľud jed-
nou z uvedených formúl. Sám
kňaz alebo jeden z posluhujúcich
na to súci môže uviesť veriacich
do omše dňa. Nech to však urobi
veľmi stručne.

87. Po úkone kajúcnosti nasleduje
vzývanie Pána a oslavná pieseň
(Glória) podľa rubrik (č. 30-31).
Oslavnú pieseň začína bud sám
kňaz, alebo speváci, alebo všetci
naraz.

88. Potom kňaz vyzve lud na mod-
litbu,hovoriac so zopätými rukami
Modlime sa. Všetci sa spolu s kňa-
zom v tichosti krátko modlia.
Nato kňaz s rozopnutými rukami
prednesie modlitbu dňa a l'ud na
jej konci zvolá Amen.

Liturgia slova

89. Po modlitbe dňa lektor ide
k ambone a prednesie prvé čítanie.
Všetci ho počúvajú sediačky a na
konci povedia aklamáciu.

90. Po čítaní žalmista, alebo kan-
tor, alebo sám lektor prednesie

žalm a lud povie príslušnú od—
poved (porov. č. 36).

91. Ak má byť pred evanjeliom
ešte druhé čítanie, lektor ho pred-
nesie pri ambone ako prvé. Všetci
sediačky počúvajú a na konci po-
vedia aklamáciu.

92. Nasleduje Aleluja alebo iný
spev, ako si to vyžaduje liturgické
obdobie (porov. č. 37-39).

93. Kým sa spieva Aleluja alebo iný
spev, kňaz naloží do kadidelnice
temian, ak sa používa kadidlo. Po-
tom so zloženými rukami sklo-
nený pred oltárom hovorí potichu
Všemohúci Bože, očisť mi srdce.

94. Vezme evanjeliár, ak je na ol-
tári, a pristúpi k ambone. Pred
ním idú posluhujúci, ktori môžu
niest' kadidelnicu a sviece.

95. Na ambone kňaz otvorí knihu
a hovorí Pán s vami a potom Čí-
tanie zo svätého evanjelia, pričom
palcom poznačí znakom kríža kni-
hu a seba na čele, na perách a na
hrudi. Potom, ak sa používa ka-
didlo, incenzuje knihu. Po akla-
mácií l'udu prednáša evanjelium.
Po skončení pobozká knihu a ho-
vori potichu Slová svätého evan—
jelia nech zmyjú naše previnenia.
Po evanjeliu nasleduje odpoveď
ľudu podl'a miestneho zvyku.

96. Ak niet lektora, sám kňaz pri
ambone prednesie všetky čítania
a ak treba aj spevy po nich. Ked'
sa používa kadidlo, pri ambone
naloží do kadidelnice temian a
potom sklonený hovorí Všemohúci
Bože, očísf mi srdce.

97. Homilia sa prednáša od sedad-
la alebo z ambony.

VŠEOBECNÉ SMERNICE - MISÁL 41*

