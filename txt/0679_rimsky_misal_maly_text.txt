znovuzrodit' z teba; *

prosíme ťa, —

posväcuj nás Duchom,

ktorý nás pretvára na tvoje deti,
a živ nás Chlebom z tvojho stola.
Skrze Krista, nášho Pána.

27. júla
sv. GORAZDA A SPOLOČNÍKOV

ÚVODNY SPEV (Porov. ž 23, 5-6)

Oslavujrne svätých, ktorí dostali požehnanie od Pána
a odmenu od Boha, svojho Spasiteľa,
lebo sú z pokolenia, ktoré hľadá Boha.

Alebo: (]er 3, 15)

Dám vám pastierov podľa môjho srdca,
budú vás viesť rozumne a múdro.

MODLITBA DNA

Všemohúci a milosrdný Bože,

ty si povzbudzoval Metodovho učeníka
Gorazda a jeho spoločníkov,

aby pokračovali v apoštolskom diele

našich vierozvestov; *

na ich orodovanie nám pomôž, —

aby sme aj my dnešnému svetu

ohlasovali blahOZVest' tvojho Syna

a upevňovali jednotu Cirkvi.

Skrze nášho Pána Ježiša Krista, tvojho Syna,
ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

27. JÚLA 579

