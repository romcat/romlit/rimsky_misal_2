1. ZA SVÁTÚ CIRKEV

Modlime sa, milovaní bratia a sestry,

za svätú Božiu Cirkev: nech jej náš Boh a Pán
láskavo udelí pokoj , jednotu a ochranu na celom svete,
aby sme mohli pokojne a nerušene oslavovať
Boha Otca všemohúceho.

Všetci sa potichu modlia na tento úmysel. Potom kňaz nahlas pokračuje:
Všemohúci a večný Bože,

ty si v Kristovi zjavil svoju slávu všetkým národom;
ochraňuj dielo svojej lásky,

aby Cirkev, rozšírená po celom svete,

vytrvala v pevnej viere a neohrozene t'a vyznávala.
Skrze Krista, nášho Pána.

O.: Amen.

11. ZA PÁPEŽA

Modlime 'sa za nášho Svätého Otca, pápeža M.,
ktorého si náš Boh a Pán vyvolil spomedzi biskupov:
nech ho chráni a zachová pre svoju Cirkev,
aby mohol spravovať svätý l'ud Boží.

Všetci sa potichu modlia na tento úmysel. Potom kňaz nahlas pokračuje:
Všemohúci a večný Bože,

od tvojej vôle závisí všetko;

láskavo vypočuj naše prosby

a ochraňuj nášho najvyššieho pastiera,

aby kresťanský ľud, ktorý si mu zveril,

pod jeho vedením rástol vo viere a láske.

Skrze Krista, nášho Pána.

O.: Amen.

162 VEĽKY TýžDEN

