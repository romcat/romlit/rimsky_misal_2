Pri druhej skúške:

Bože dobrý a láskavý,

duchovne dvíhaj rodinu svojich veriacich: *
kajúcich usmerňuj,

tebe oddaných ochraňuj

a všetkých nás sprevádzaj svojou dobrotou, -
aby sme dosiahli spásu.

Skrze Krista, nášho Pána.

Pri tretej skúške:

Milosrdný Bože,

prišli sme k tebe s úprimným a oddaným srdcom ; *
vypočuj naše prosby za katechumenov —

a nás všetkých ochráň pred každým nepokojom,
aby sme sa mohli ustavične tešiť z našej spásy.
Skrze Krista, nášho Pána.

3. PRI UDEĽOVANÍ KRSTU

Tento omšový formulár sa používa pri krste dospelých, najmä keď sa
po krste vysluhuje aj sviatosť birmovania. Možno ho použiť aj pri krste detí.
Omša sa slávi v bielom rúchu

Formulár omše krstu sa môže brať vždy okrem nediel' adventných,
pôstnych a veľkonočných, okrem slávností, Popolcovej stredy a celého
Veľkého týždňa.

A

ÚVODNY SPEV (Ef 4, 24)

Máte si obliecť nového človeka,
stvoreného podl'a Božieho obrazu v spravodlivosti a pravej svätosti.

MODLITBA DNA

Bože a Otče náš,
vo sviatosti krstu nám dávaš účasť

77s OMŠE PRI VYSLUHOVANÍ SVIATOSTÍ

