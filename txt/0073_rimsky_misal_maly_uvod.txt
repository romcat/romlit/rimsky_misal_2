APOŠTOLSKY LIST
VYDANY z VLASTNÉHO PODNETU
(MOTU PROPRIO)

KTORYM SA SCHVAĽUJÚ
VŠEOBECNÉ SMERNICE
LITURGICKÉHO ROKU

A Nový RÍMSKY VŠEOBECNY KALENDÁR

PÁPEŽ PAVOL VI.

Sláveniu veľkonočného tajomstva, ktoré
sa rozvíja v jednotlivých dňoch i týždňoch
a cez celý rok, patrí v náboženskom živote
kresťanov naiprednejšie miesto, ako jasne
učí Druhý vatikánsky posvätný koncil.
Preto pri obnove liturgického roku, pre
ktorú tento koncil určil príslušné normy,1
treba toto Kristovo veľkonočné tajomstvo
dôkladnejšie ozrejmit' tak predpismi o
jednotlivých obdobiach a o rozličných svä-
tých, ako aj novou úpravou rímskeho
kalendára.

I.

Postupom času totiž pre nadmerný počet
vigílií a sviatkov s oktávami a vsunutím
rozličných častí liturgického roku boli ve-

1 Porov. Druhý vatik. koncil, konšt. () posv. liturgii Sacroranttum
Contilium, č. [oz-ru.

APOŠTOLSKY LIST 73*

