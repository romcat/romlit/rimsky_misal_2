SPEV NA PRIJÍMANIE (Mt 28, 30)

Hľa, ja som s vami po všetky dni
až do skončenia sveta. Aleluia.

PO PRIJÍMANÍ

Všemohúci a večný Bože,

zmŕtvychvstaním tvojho Syna

znovu si nám otvoril cestu do večného života; *
zveľaďuj v nás účinky veľkonočného tajomstva —
a naplň nás silou sviatostněho pokrmu,

ktorý sme prijali.

Skrze Krista, nášho Pána.

 

PIATOK
po Štvrtej veľkonočnej nedeli

 

ÚVODNY SPEV (Ziv ;, 9-xo')

Vykúpil si nás svojou krvou, Pane,

z každého kmeňa, jazyka, ľudu a národa
a urobil si nás nášmu Bohu

kráľovským kňazstvom. Aleluja.

MODLITBA DNA

Dobrotivý Bože,

pôvodca našej slobody a spásy,

ty si nás vykúpil predrahou krvou svojho Syna; *
vrúcne ťa prosíme, —

buď zdrojom nášho života,

aby sme v tebe vždy nachádzali trvalú radosť a istotu.
Skrze nášho Pána ]ežiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

240 VEĽKONOČNÉ OBDOBIE

