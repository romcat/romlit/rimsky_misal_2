NAD OBETNYMI DARMI

Bože, v pokore ti prinášame obetu

za spásu tvojho služobníka M., (tvojej služobnice M.,)
ktorý(á) veril(a), že tvoj Syn je láskavý Spasiteľ; *
prosíme ťa, —

nech v ňom nájde aj milosrdného Sudcu,

ktorý žije a kraľuje na veky vekov.

Pieseň vďaky o zosnulých (str. 399 n.).

SPEV NA PRIJÍMANIE (Porov. 4 Ezd :, 35.34)

Svetlo večné nech svieti zosnulým, Pane,
v spoločenstve svätých, lebo si dobrotivý.
Odpočinutie večné daj im, Pane,

a svetlo večné nech im svieti

v spoločenstve svätých, lebo si dobrotivý.

PO PRIJÍMANÍ

Pane a Bože náš,

tvoj Syn nám vo svojom sviatostnom tele
zanechal pokrm na cestu do večnosti; *
prosíme ťa vrúcne, —

daj, aby náš brat M. (naša sestra M.)
účinkom tohto pokrmu

došiel (došla) až k večnej hostine

tvojho Syna, Ježiša Krista,

ktorý žije a kraľuje na veky vekov.

B. MIMO VEĽKONOČNÉHO OBDOBIA

ÚVODNY SPEV

Kiež mu (jej) Pán otvorí bránu raja a vovedie ho (ju)
do večného domova, kde už niet smrti, ale len večná radosť.

958 OMŠE ZA ZOSNULYCH

