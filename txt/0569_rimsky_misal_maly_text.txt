18. NA VŠECHSVÁTYCH

Kňaz: Dobrotivý Boh, ktorý priviedol svätých k več-
nej radosti a dokonalému šťastiu, nech aj vás požeh-
náva a chráni od zlého.

Ľud: Amen.

Kňaz: Nech vás svätí povzbudzujú svojím príkla-
dom a nech vám svojím orodovaním pomáhajú
ochotne slúžiť Bohu i blížnemu.

Ľud: Amen.

Kňaz: Nech vás Boh sprevádza svojou milosťou a
privedie vás do oslávenej Cirkvi, aby ste sa v nebi
večne radovali so všetkými vyvolenými.

Ľud: Amen.

Kňaz: Nech vás žehná všemohúci Boh, Otec i Syn
% i Duch Svätý.
Ľud: Amen.

PRI R ČZNYCH PRÍLEŽITOSTIACH

19. PRI POSVIACKE CHRÁMU

Biskup (kňaz): Všemohúci Boh, ktorý chcel všetko
zjednotiť v Kristovi a urobil ho uholným kameňom
svojej Cirkvi, nech vám dá milosť, aby ste mohli
stále byť živými kameňmi duchovného chrámu,
ktorým je Cirkev.

Ľud: Amen.

Biskup (kňaz): Pán neba i zeme, ktorý nás dnes
zhromaždil k (výročnej) slávnosti posviacky tohto
chrámu, nech Vás zahrnie hojným požehnaním.
Ľud: Amen.

SLÁVNOSTNÉ POŽEHNANIA 469

