môžu aj lektori laici; v tomto prípade, ak je možné, treba vyhradiť kňa-
zovi časti, v ktorých hovorí Kristus.

Diakoni (nie však iní) pred spievaním pašií prosia od kňaza požehna-
nie, ako sa to robí vo svätej omši pred evanjeliom.

23. Po pašiách, ak je to vhodné, nech je krátka homília.

Vyznanie viery.

24. NAD OBETNYMI DARMI

Milostivý Otče, *

pre umučenie tvojho Syna

preukáž nám svoje zľutovanie,

ktoré si nezasluhujeme svojimi skutkami, _-
ale očakávame ho od tvojho milosrdenstva
ako ovocie tejto vznešenej obety.

Skrze Krista, nášho Pána.

25 . PIESEN VĎAKY

: Pán s vami.

: I s duchom tvojím.

; Hore srdcia.

: Máme ich 11 Pána.

: Vzdávajme vďaky Pánovi, Bohu našmu.
: Je to dôstojné a správne.

.0.<9.<.0.<

Je naozaj dôstojné a správne, dobré a spásonosné
vzdávať vďaky vždy a všade

tebe, Pane, svätý Otče, všemohúci večný Bože,
skrze nášho Pána Ježiša Krista.

Lebo on, hoci-* bol nevinný,

dobrovoľne trpel za nás vinníkov

a nechal sa nespravodlivo odsúdiť za hriešnikov.
Svojou smrťou zotrel naše hriechy

a svojím zmŕtvychvstaním získal nám nový život.

142 VEĽKY TYŽDEN

