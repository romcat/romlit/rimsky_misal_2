PO PRIJÍMANÍ

Bože a Otče náš,

pri oslave svätej M.

zahrnul si nás svojimi darmi; *
prosíme ťa, _

nech sviatostná milosť zotrie naše viny
a posilní nás v životnom boji.

Skrze Krista, nášho Pána.

 

ÚVODNÉ SPEVY,
ktoré možno ľubovoľne brať
na slávností a sviatky

 

1. Radujme sa všetci v Pánovi
a slávme sviatok
na počesť svätého M. (svätej M.), (mučeníka, mu-
čenice, biskupa, kňaza...),
z jeho (z jej) oslávenia (sviatku, mučeníctva) sa
radujú anjeli
a spolu s ním (s ňou) velebia Syna Božieho.

N

. Radujme sa všetci v Pánovi
na slávnosť (sviatok) svätého M., nášho ochrancu,
ktorý prešiel dnes 2 tohto sveta do neba
ozdobený víťazstvom viery,
aby s Kristom kraľoval naveky.

ĽUBOVOĽNÉ ÚVODNÉ SPEVY 769

