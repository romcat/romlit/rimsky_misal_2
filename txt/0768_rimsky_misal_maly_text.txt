25 . novembra

SV. KATARÍNY ALEXANDRIJSKE],
PANNY A MUČENICE

Spoločná omša mučeníkov (str. 713 11.)
alebo panien (str. 746 n.).

MODLITBA DNA

Bože, prameň múdrosti a sily,

svojim verným zj avuješ moc svojho milosrdenstva; *
na príhovor svätej Kataríny nám pomáhaj

obstáť v skúškach života —

a V každej núdzi nám daj pocítiť tvoju láskavú pomoc.
Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

30. novembra

SV. ONDREJA, APOŠTOLA

Sviatok
(v Košickej diecéze sviatok hlavného patróna)

ÚVODNY SPEV (Porov. Mt 4, x8-19)

Na brehu Galilejského mora

Pán uzrel dvoch bratov Petra a Ondreja.

I povedal im:

Poďte za mnou a ja z vás urobím rybárov ľudí.

Oslavná pieseň.

MODLITBA DNA

Všemohúci Bože,
svojej Cirkvi si dal svätého apoštola Ondreja
za zvestovateľa viery a duchovného pastiera; *

668 VLASTNÉ OMŠE SVÁTYCH

