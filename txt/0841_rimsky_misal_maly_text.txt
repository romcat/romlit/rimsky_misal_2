Alebo: (Jn 15, 4-5)

Pán Ježiš hovorí:

Ostaňte vo mne a ja zostanem vo vás;
kto ostáva vo mne a ja v ňom,
prináša veľa ovocia.

PO PRIJÍMANÍ

Pane a Bože náš, *

daj, aby prijatá sviatosť

posilňovala v nás vieru, ktorú hlásali apoštoli —
a ktorú svätý M. starostlivo chránil

a horlivo rozširoval.

Skrze Krista, nášho Pána.

12. Omša o misionároch

ÚVODNY SPEV (Ž 95. 3-4)

Zvestujte Božiu slávu pohanom
a jeho zázraky všetkým národom,
lebo vel'ký je Pán a veľkej chvály hoden.

MODLITBA DNA

Milosrdný Bože,

ty si poslal svätého M. ohlasovať

Kristovo nevyspytateľné bohatstvo; *

na jeho príhovor nám pomáhaj

vždy lepšie ťa poznávať, -—

aby sme verne žili podľa pravdy evanjelia

a prinášali ovocie dobrých skutkov.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

DUCHOVNýCH PASTIEROV 741

