Potom biskup vystrie ruky nad ľud a modlí sa:

Bože, utvrď v nás svoje dielo

a v srdciach svojich veriacich

ochraňuj dary Ducha Svätého, *

aby sa nehanbili

pred svetom vyznávať ukrižovaného Krista —
a aby s oddanou láskou plnili jeho príkazy.
Skrze Krista, nášho Pána.

O.: Amen.

Nech vás žehná všemohúci Boh,
Otec i Syn % i Duch Svätý.
O.: Amen.

B

ÚVODNY SPEV (Porov. Rím 5, 5; 8, n)

Božia láska sa rozlieva v našich srdciach
skrze Ducha Svätého, ktorý prebýva v nás.

MODLITBA DNA

Bože, prosíme ťa,

láskavo zošli na nás Ducha Svätého, *

aby sme všetci žili v jednej viere,

aby nás posilňovala moc jeho lásky, —

a tak aby sme dospeli

k miere plného vzrastu Kristovho,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.
Možno použiť aj iné modlitby (pozri str. 783 a 787).

NAD OBETNýMI DARMI

Milostivý Bože, *
prijmi so svojím milovaným Synom

786 OMŠE PRI VYSLUHOVANÍ SVIATOSTÍ

