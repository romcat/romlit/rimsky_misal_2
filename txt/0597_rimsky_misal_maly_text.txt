SVÁTÁ OMŠA
ÚVODNY' SPEV (Ž 47, 10-11)

Pripominame si, Bože, tvoje milosrdenstvo
uprostred tvojho chrámu.

Ako tvoje meno, Bože, tak aj tvoja sláva
šíri sa až do končín zeme;

tvoja pravica je plná spravodlivosti.

Oslavná pieseň.

MODLITBA DNA

Všemohúci a večný Bože,

tvoj jednorodený Syn

prijal našu ľudskú prirodzenosť

a dnes ti bol obetovaný v chráme; *
pokorne ťa prosíme, -

obnov náš život a naše zmýšľanie,

aby sme aj my mohli s čistým srdcom
predstúpit' pred tvoju tvár.

Skrze nášho Pána Ježiša Krista, tvojho Syna,
ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

Vyznanie viery sa vynechá.

NAD OBETNYMI DARMI

Večný Otče,

ty si chcel, aby sa tvoj jednorodený Syn
obetoval za spásu sveta

ako nepoškvrnený Baránok; *

prosíme ťa, —

prijmi láskavo aj tento obetný dar svojej Cirkvi,
ktorá t'a s radosťou oslavuje.

Skrze Krista, nášho Pána.

2. FEBRUÁRA 497

