SPEV NA PRIJÍMANIE (Jn 14, 18; 16, za)

Pán Ježiš hovorí: Nenechám vás ako siroty;
pridem k vám, a srdce sa vám zaraduje. Aleluia.

PO PRIJÍMANÍ

Prosíme ťa Bože, *

dobrotivo ochraňuj svoj ľud,

ktorý si nasýtil sviatostným pokrmom, —
a pomáhaj mu,

aby prešiel z temnoty hriechu

do nového života.

Skrze Krista, nášho Pána.

 

UTOROK
po Siedmej veľkonočnej nedeli

 

ÚVODNY SPEV (Zjv :, I7-I8)

Ja som Prvý a Posledný a Žijúci.
Bol som mŕtvy, a hľa, žijem na veky vekov. Aleluja.

MODLITBA DNA

Všemohúci a milosrdný Bože, *

prosíme ťa, zošli nám Ducha Svätého, —

aby v nás prebýval svojou milosťou

a urobil nás chrámom svojej slávy.

Skrze nášho Pána Ježiša Krista, tvojho Syna,
ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky vekov.

UTOROK PO SIEDME] NEDELI 265

