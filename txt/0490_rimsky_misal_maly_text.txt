Lebo teba oslavujú zástupy svätých

a keď korunuješ ich zásluhy,

korunuješ dielo svojej milosti.

Ich život nám dávaš za príklad,

na ich orodovanie nám pomáhaš

a v spoločenstve s nimi utváraš z nás jednu rodinu.
Ich hrdinské svedectvo nám dáva silu,

aby sme víťazne obstáli v duchovnom zápase

a spolu s nimi dosiahli nevädnúci veniec slávy
skrze nášho Pána Ježiša Krista.

Preto ti s anjelmi aj archanjelmi
spolu so všetkými svätými
spievame pieseň chvály

a radostne voláme:

Svätý, svätý, svätý Pán Boh všetkých svetov...

O SVÁTYCH II
0 príklade a orodovaní svätých

Táto pieseň vďaky sa používa vo votívnej omši o všetkých svätých,
v omšiach o svätých patrónoch, o titulárnych svätcoch kostolov, ďalej
na slávností a sviatky svätých, ak nie je predpísaná vlastná pieseň vdaky.
Môže sa používať aj v omšiach, keď sa slávi spomienka na svätých.

: Pán s vami.

: I s duchom tvojim.

: Hore srdcia.

: Máme ich u Pána.

: Vzdávajme vďaky Pánovi, Bohu nášmu.
: je to dôstojné a správne.

PŠPŠPŠ

Je naozaj dôstojné a správne,
všemohúci a večný Bože,

390 PIESNE VĎAKY

