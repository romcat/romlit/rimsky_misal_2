SPEV NA PRIJÍMANIE

— Ak sa číta evanjelium o slepom od narodenia:

]ežiš mi potrel oči,
išiel som, umyl som sa, otvorili sa mi oči,
a uveril som v Boha.

— Ak sa číta evanjelium o márnotratnom synovi:

Syn môj, treba sa radovať,
lebo tvoj brat bol mŕtvy, a ožil,
bol stratený, a našiel sa.

— Ak sa číta iné evanjelium:

Jeruzalem je vystavaný ako mesto
spojené v jeden celok.

Tam prichádzajú kmene, kmene Pánove,
aby velebili meno Pánovo.

PO PRIJÍMANÍ

Všemohúci Bože,
ty osvecuješ každého človeka
prichádzajúceho na tento svet; *

(Porov. Jn 9, II)

(Lk 15, 32)

(Ž rzr, 3-4)

prosíme ťa, osviet' nás žiarou svojej milosti, —

aby sme vždy mali na mysli to,
čo sa zhoduje s tvojou vôľou,

a milovali t'a s úprimným srdcom.
Skrze Krista, nášho Pána.

 

PONDELOK

po Štvrtej pôstnej nedeli

 

ÚVODNY SPEV

Ja dúfam v Pána;
plesám a teším sa, že si milosrdný,
lebo si zhliadol na moju poníženosť.

(Ž 30, 7-8)

PONDELOK PO ŠTVRTE] NEDELI 113

