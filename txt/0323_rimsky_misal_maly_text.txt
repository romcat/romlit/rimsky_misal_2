MODLITBA DNA

Láskavý Otče,

ty si nás skrze svojho Syna vykúpil

a prijal za svoje milované deti; *

sprevádzaj s otcovskou láskou všetkých,

čo veria v Krista, —

aby dosiahli pravú slobodu

a večný život.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Milosrdný Bože, *

láskavo posvät' tieto obetné dary,

ktoré predstavujú našu duchovnú obetu, —

a urob aj z nás večný dar pre teba.

Skrze Krista, nášho Pána.

Pieseň vďaky veľkonočné (str. 367 n.).

SPEV NA PRIJÍMANIE (Jn 17, 24)

Otče, chcem, aby aj tí, ktorých si mi dal, boli so mnou tam, kde som ja,
aby uzreli moju slávu, ktorú si mi dal. Aleluja.

PO PRIJÍMANÍ

Dobrotivý Otče,

posilnení sviatostným pokrmom pokorne prosíme, *
aby Eucharistia,

ktorú nám tvoj Syn prikázal sláviť

na svoju pamiatku, —

rozmnožila v nás lásku k tebe a k blížnemu.
Skrze Krista, nášho Pána.

SOBOTA PO DRUHÉ] NEDELI 223

