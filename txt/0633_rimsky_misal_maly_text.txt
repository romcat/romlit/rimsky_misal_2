obdivujeme jeho záslužné činy

a vrúcne t'a prosíme, pomáhaj nám, -—

aby sme vždy nasledovali jeho stálosť vo viere.
Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

20. mája
SV. BERNARDÍNA SIENSKÉHO, KNAZA

Spoločná omša duchovných pastierov-misionárov (str. 738 n.).

MODLITBA DNA

Láskavý Otče,

ty si vložil do srdca svojho kňaza Bernardína
veľkú lásku k svätému menu Ježiš; *

pre jeho zásluhy a na jeho orodovanie dopraj , —
aby sme aj my horeli láskou k tebe.

Skrze nášho Pána ]ežiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

25. mája

SV. BÉDU CTIHODNÉHO,
KNAZA A UČITEĽA CIRKVI

Spoločná omša učiteľov Cirkvi (str. 743 n.)
alebo svätých rehoľníkov (str. 759 n.).

25. MÁJA 533

