Pokorne ťa prosíme, Pane, ochra-
ňuj nás ako svoj vyvolený ľud a
pomáhaj nám úprimne uctievať
tvoje sväté meno, aby sme si
zaslúžili dary tvojho milosrden-
stva. Ty žiješ a kraluješ na veky
vekov.

P ČSTNE OBDOBIE

ÚVODNÁ MODLITBA

a) Drahí bratia a sestry, v pôstnom
čase, čase milosti a spásy, prosme
za väčšie posvätenie seba a celého
sveta.

b) Drahí bratia a sestry, prosme
pokorne Pána, aby nás priviedol
na svoj svätý vrch duchovného ži-
vota.

c) Drahí bratia a sestry, modlime
sa k Bohu Otcovi, ktorý nás chce
spasiť, aby sme sa vedeli zrieknuť
zlého a konať, čo je dobré v jeho
očiach.

ÚMYSLY

a) * Za svätú Cirkev — aby sa
počas štyridsaťdenne'ho pôstu očis-
tila a väčšmi zdokonalila, prosme
Pána.

* Za pápeža, biskupov a kňazov
— aby slovom i príkladom viedli
veriacich ku spásonosnej kajúcnos-
ti, prosme Pána.

* Za pápeža, biskupov a kňazov
— aby účinne roznecovali v Bo-
žom l'ude vieru v Ježiša Krista a
záujem o Božie slovo, prosme Pána.
* Za spovedníkov — aby pri vyslu-
hovaní sviatosti zmierenia boli po-
korní, milosrdní a láskaví, prosme
Pána.

b) * Za tých, čo vládnu národom
— aby múdrou výchovou odvá-
dzali lud od násilia, sebectva a ne-
mravnosti, prosme Pána.

* Za celý svet — aby v ňom zavlá-
dol pokoj, žeby Cirkcv mohla ne-
rušene hlásať evanjelium a vyslu-
hovať sviatosti, prosme Pána.

* Za všetkých ľudí — aby sa za-
mysleli nad pominuteľnosťou po-
zemského života a nesmrteľnosťou
duše, a dôsledne podľa toho žili,
prosme Pána.

c) * Za kresťanský l'ud — aby sa
v tejto posvätnej dobe väčšmi
živil Božím slovom, prosme Pána.
* Za nedbalých a povrchných kres-
ťanov — aby sa zamysleli nad
milost'ou krstu, ktorým boli včle-
není do Krista, a rozhodli sa horli-
vo plniť Božiu vôlu, prosme Pána.
* Za všetkých malomyseľných a
znechutených — aby sa vzchopili
myšlienkou na žiarivé Kristovo vi-
ťazstvo a našli životnú silu a odhod-
lanosť v nádeji na večný život
s nim, prosme Pána.

* Za všetkých chorých, núdznych,
trpiacich a prenasledovaných —
aby im dobri ľudia účinnou brat-
skou pomocou spritomňovali Božiu
dobrotu, prosme Pána.

d) * Za nás všetkých —— aby sme
radi chodievali do chrámu na bo-
hoslužby a aby sme uchovávali
v čistote a svätosti chrám nášho tela,
ktoré má byť vzkriesené a oslávené
s Kristom, prosme Pána.

* Za nás všetkých — aby sme po-
hotove odpovedali na Božie výzvy
k svätosti, pripodobňovali sa stále

MODLITBA VERIACICH 1021

