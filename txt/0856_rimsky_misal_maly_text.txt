na tvojom božskom živote
v spoločenstve svätých V nebi.
Skrze Krista, nášho Pána.

4

ÚVODNY SPEV (Porov. Mal 2, 6)

Ústa svätého M. podávali pravdivé učenie
a na jeho perách nebolo klamu;

pokojne a spravodlivo kráčal s Bohom

a mnohých odvrátil od hriechu.

MODLITBA DNA

Milosrdný Bože,

ty vieš, že sme krehkí a ľahko klesáme; *
prosíme t'a, zl'utuj sa nad nami —-

a príkladom svojich svätých

posilní v nás lásku k tebe.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Nekonečný Bože, *

na sviatok svätého M. prinášame ti dary
a prosíme ťa, —

aby nás táto svätá obeta zmierila s tebou

a pomohla nám dosiahnuť spásu.
Skrze Krista, nášho Pána.

SPEV NA PRIJÍMANIE (Mt ;. 8—9)

Blahoslavení čistého srdca, lebo oni uvidia Boha;
blahoslavení, čo šíria pokoj,

lebo ich budú volať Božími synmi;

blahoslavení, ktorých prenasledujú pre spravodlivosť,
lebo ich je nebeské kráľovstvo.

756 SPOLOČNÉ OMŠE

