ktorí ako krstní rodičia

privedú našich katechumenov

k sviatosti znovuzrodenia.

Pamätaj i na všetkých tu prítomných...

Bože, milostivo prijmi túto obetu,

ktorú ti predkladáme my, tvoji služobníci,

i celá tvoja rodina.

Prinášame ti ju aj za tvojich služobníkov a služobnice,
ktorých si vyvolil a povolal,

aby prijali tvoju milosť

a dosiahli večný život.

(Skrze nášho Pána ]ežiša Krista. Amen.)

SPEV NA PRIJÍMANIE

— Ak sa číta evanjelium o Samaritánke: (Jn 4, 13-14)
Pán Ježiš povedal:

Kto bude piť z vody, ktorú mu ja dám,

stane sa mu prameňom vody prúdiacej do života večného.

— Ak sa číta evanjelium o slepom od narodenia: (Porov. ]n 9, 11)
Pán Ježiš mi potrel oči,

išiel som, umyl som sa, otvorili sa mi oči,

a uveril som v Boha.

— Ak sa číta evanjelium o Lazárovi: (Jn II, 26)
Pán Ježiš povedal:
Každý, kto žije a verí vo mňa, neumrie naveky.

PO PRIJÍMANÍ

pri prvej skúške:

Dobrotivý Bože,

ochraňuj týchto katechumenov *

a daj, nech v nich pôsobí dielo vykúpenia, -—
aby sa dôstojne pripravili na prijatie sviatostí,
ktoré dávajú večný život.

Skrze Krista, nášho Pána.

PRÍPRAVNÉ SKÚŠKY NA KRST 777

