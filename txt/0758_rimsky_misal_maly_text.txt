9. novembra

VYROČIE POSVÁTENIA
LATERÁNSKE] BAZILIKY

Sviatok

Spoločná omša na výročie posvätenia chrámu (str. 688 alebo 69x).

10. novembra

SV. LEVA VEĽKÉHO,
PÁPEŽA A UČITEĽA CIRKVI

Spomienka

Spoločná omša duchovných pastierov-pápežov (str. 725 n.)
alebo učiteľov Cirkvi (str. 745 n.).

MODLITBA DNA

Vševládny Bože,

svojej Cirkvi si dal pevný základ

na skale, ktorou je Peter,

a nikdy nedovolíš,

aby ju premohli pekelné mocnosti; *

na príhovor svätého pápeža Leva Veľkého
posilňuj ju v pravej viere —

a zachovaj v jednote a pokoji.

Skrze nášho Pána Ježiša Krista, tvojho Syna,
ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Milosrdný Bože, *
pre túto svätú obetu
ukazuj svojej Cirkvi správnu cestu,

658 VLASTNÉ OMŠE SVÁTYCH

