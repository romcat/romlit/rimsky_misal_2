MODLITBA DNA

Dobrotivý Bože, ty si povolal svätého Ireneja
obraňovať pravé učenie

a upevňovať pokoj V Cirkvi; *

prosíme t'a, —-

na jeho príhovor obnov v nás vieru a lásku,

aby sme sa vždy usilovali o jednotu a svornost'.
Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

V jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Veľký a večný Bože,

s radosťou ti prinášame svätú obetu

v deň víťaznej smrti svätého Ireneja; *
nech táto svätá omša oslávi tvoje meno
a nás nech upevní v láske k pravde, —
aby sme sa držali neporušenej viery

a zachovali jednotu Cirkvi.

Skrze Krista, nášho Pána.

PO PRIJÍMANÍ

Milosrdný Bože,

týmito posvätnými tajomstvami

zveľaďuj v nás vieru,

za ktorú tvoj svätý biskup Irenej
podstúpil mučenícku smrť, *

a pomáhaj nám, —

aby sme aj my dôsledne žili podľa Viery
a dosiahli slávu v nebi.

Skrze Krista, nášho Pána.

zs. JÚNA 555

