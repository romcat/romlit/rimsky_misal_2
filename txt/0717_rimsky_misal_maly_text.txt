V.: Hore srdcia.

O.: Máme ich u Pána.

V.: Vzdávajme vďaky Pánovi, Bohu nášmu.
O.: Je to dôstojné a správne.

Je naozaj dôstojné a správne, dobré a spásonosné
vzdávať vďaky vždy a všade

tebe, Pane, svätý Otče, všemohúci a večný Bože.
Lebo vykúpenie ľudstva sa z tvojho rozhodnutia
uskutočnilo na dreve kríža.

Zo stromu v raji prišla na nás smrť,

zo stromu kríža dostali sme nový život.

Nepriateľ, ktorý nás v raji premohol,

na dreve kríža bol premožený Kristom, naším Pánom.

Skrze neho tvoju velebu chvália anjeli,

klaňajú sa ti všetky nebeské zástupy,

i blažení serafíni ta oslavujú spoločným plesaním.
Prosíme, dovol' i nám,

aby sme t'a s nimi v pokornej úcte chválili:

Svätý, svätý, svätý Pán Boh všetkých svetov...

SPEV NA PRIJÍMANIE (]n m, 32)

Pán ]ežiš povedal:
Keď budem vyzdvihnutý od zeme,
všetkých pritiahnem k sebe.

PO PRIJÍMANÍ

Pane ]ežišu Kriste,

na dreve kríža zaslúžil si nám nový život

a v tejto sviatosti

nám dávaš za pokrm seba samého; *

vrúcne t'a prosíme, -—

daj nám účast' aj na sláve svojho zmŕtvychvstania.
Lebo ty žiješ a kraľuješ na veky vekov.

14. SEPTEMBRA 617

