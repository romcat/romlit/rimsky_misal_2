svojom prijímaní, ale pred prijí-
maním veriacich.

j) Po rozdaní prijímania kňaz a
veriaci sa podľa okolností chvílu
v tichosti modlia. Keď sa uzná za
dobré, celé zhromaždenie môže
spievať aj nejaký hymnus, žalm,
alebo iný chválospev.

k) V modlitbe po prijímaní kňaz
prosí o ovocie sláveného tajomstva.

Ľud si prisvojuje modlitbu zvola—
ním Amen.

D) Záverečné obrady

57. Záverečné obrady pozostávajú:
a) z kňazovho pozdravu a po-
žehnania; v niektoré dni a pri
niektorých príležitostiach sa toto
požehnanie bohatšie vyjadruje
modlitbou nad ľudom alebo inou
slávnostnejšou formulou;

b) 2 prepustenia, ktorým sa
zhromaždenie rozpúšťa, aby sa kaž-
dý vrátil k svojej statočnej práci,
chválil Pána a ďakoval mu.

Hlava III
POVINNOSTI A SLUŽBY PRI OMŠI

58. V zhromaždení, ktoré sa schá-
dza na omšu, každý má právo a po-
vinnosť prispieť svojou účasťou
rozličným spôsobom podľa roz-
dielnosti svätenia a poslania.!15 A
tak všetci, či už posluhujúci alebo
veriaci, kedykoľvek konajú svoju
službu, nech konajú všetko to, a
iba to, čo im prislúcha,“ aby sa
zo slávenia bohoslužby a jej uspo-
riadania zračilo, že v Cirkvi sú
rozličné svätenia a služby.

1. O povinnostiach a službách
vysvätených osôb

59. Každé zákonité slávenie Eu-
charistie riadi biskup osobne alebo
prostredníctvom kňazov, svojich
pomocníkov." Keď je biskup prí-
tomný na omši, ktorá sa slávi za
účasti ľudu, patrí sa, aby on pred-
sedal zhromaždeniu a pri slávení

omše si pridružil kňazov a podľa
možnosti s nimi koncelebroval.

Je to tak nielen preto, aby sa
zvýšila vonkajšia slávnostnost'
obradu, ale aby sa tým v pln-
šom svetle vyjadrilo tajomstvo Cir-
kvi, ktorá je sviatosťou jednoty.48

Ak však biskup neslávi Eucha-
ristiu, ale poveruje tým iného,
vtedy je vhodné, aby sám viedol
liturgiu slova a na konci omše
udelil požehnanie.

60. Aj kňaz, ktorý z moci svätenia
má právo v spoločenstve veriacich
prinášať obetu ako Kristov zástup-
ca,49 stojí na čele zídeného zhro-
maždenia, predsedá pri jeho mod-
litbe, ohlasuje mu posolstvo spásy,
spája sa s ľudom, keď skrze Krista
v Duchu Svätom prináša obetu
Bohu Otcovi, svojim bratom dáva
chlieb večného života a sám má

“ Porov. Druhý vatik. koncil, konšt. !) posv. liturgii Sacrosantmm Conrilium, č. 14, 26.

“ Porov. tamže, č. 28.

" Porov. Druhý vatik. koncil, dogm. konšt. o Cirkvi Lumen gentium, č. 26, 28; konšt. o posv. liturgii

Sacrosam'lum Cnnciľíum, č. 41.

“* Porov. Druhý valik. koncil, konšt. o posv. liturgii Sucroranttum Concih'um, č. 26.
" Porov. Druhý vatik. koncil, dekrét o službe a živote kňazov Presbyterorum ordiníx, č. :; dogm.

konšt. o Cirkvi Lumen gentíum, č. 28.

36* VŠEOBECNÉ SMERNICE - MISÁL

