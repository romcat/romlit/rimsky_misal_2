PO PRIJÍMANÍ

Dobrotivý Otče, vypočuj naše prosby, *
aby nás účasť na svätých tajomstvách
nášho vykúpenia

posilňovala v tomto živote —

a priviedla do večnej radosti.

Skrze Krista, nášho Pána.

STREDA
po Druhej veľkonočnej nedeli

 

ÚvonNý SPEV (Ž 17, so; 21, 23)

Pane, budem ťa velebiť medzi národmi;
tvoje meno chcem zvestovať svojim bratom. Aleluja.

MODLITBA DNA

Dobrotivý Bože,

tajomstvom umučenia a zmŕtvychvstania tvojho Syna
obnovil si pôvodnú dôstojnosť človeka

a dal si nám nádej na slávne vzkriesenie; *
prosíme ťa, pomáhaj nám, —

aby sme činorodou láskou dosvedčovali tajomstvo,
ktoré každoročne s vierou slávime.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Vznešený Bože,

z tvojej dobrotivosti slávime svätú obetu,
ktorou nám dávaš účasť na svojom živote, *
a prosíme ťa, —

STREDA PO DRUHÉ] NEDELI 219

