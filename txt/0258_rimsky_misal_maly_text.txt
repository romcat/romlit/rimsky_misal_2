pozdvihne oči

pozdvihol oči k nebu,

k tebe, Bohu, svojmu všemohúcemu Otcovi,
vzdával ti vďaky a dobrorečil, lámal chlieb
a dával svojim učeníkom, hovoriac:

trocha sa skloní

VEZMITE A JEDZTE Z NEHO VŠETCI:
TOTO JE MOJE TELO,
KTORÉ SA OBETUJE ZA VÁS.

Ukáže konsekrovanú hostiu ľudu, znova ju položí na paténu a pokľaknu-
tim adoruje.

Potom pokračuje :

Podobne po večeri

vezme kalich,
drží ho trocha pozdvihnutý nad oltárom a pokračuje:

vzal do svojich svätých a ctihodných rúk
tento preslávny kalich,

znova ti vzdával vďaky, dobrorečil

a dal ho svojim učeníkom, hovoriac:

trocha sa skloni

VEZMITE A PITE Z NEHO VŠETCI:
TOTO JE KALICH MOJEJ KRVI,
KTORÁ SA VYLIEVA ZA VÁS
I ZA VŠETKYCH
NA ODPUSTENIE HRIECHOV.

JE TO KRV NOVEJ A VECNEJ ZMLUVY.
TOTO ROBTE NA MOJU PAMIATKU.

Ukáže kalich ludu, znova ho položí na korporál a pokľaknutím adoruie.

Potom povie :

Hľa, tajomstvo viery.

Pokračuje ako zvyčajne (pozri str. 414).

158 VEĽKY TYŽDEN

