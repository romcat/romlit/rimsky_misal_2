 

21. decembra

 

ÚVODNY SPEV (Porov. Iz 7, 14; 8, ro)

Čoskoro príde Pán a Vládca;
bude sa volať Emanuel, čo znamená: Boh je s nami.

MODLITBA DNA

Dobrotivý Otče, *

láskavo vyslyš prosby svojho ľudu,

ktorý s radosťou očakáva príchod

tvojho jednorodeného Syna v ľudskom tele; -—
daj, aby sme dosiahli odmenu večného života,
keď znova príde vo svojej sláve.

Lebo on je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Všemohúci Bože, *

dobrotivo prijmi dary,

ktoré si nám poskytol,

aby sme ti ich mohli priniesť na obetu, —

a svojou božskou mocou ich premeň

na sviatosť našej spásy. Skrze Krista, nášho Pána.
Pieseň vďaky adventná II. (str. 355).

SPEV NA PRIJÍMANIE (Lk 1, 45)

Blahoslavená si, lebo si uverila,
že sa splní, čo ti Pán povedal.

PO PRIJÍMANÍ

Milosrdný Bože,

účasť na eucharistickej obete

nech je tvojmu ľudu ustavičnou ochranou; *

34 ADVENTNÉ OBDOBIE

