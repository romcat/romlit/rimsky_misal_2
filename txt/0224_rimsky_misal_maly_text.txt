 

PONDELOK
po Piatej pôstnej nedeli

 

ÚVODNý SPEV (Ž 55, z)

Zmiluj sa nado mnou, Bože,
lebo ludia ma prenasledujú,
deň čo deň ma napádajú a utláčajú.

MODLITBA DNA

Milosrdný Bože,

vo svojej nevýslovnej láske

nás zahrnuješ hojným požehnaním; *

prosíme ťa, pomáhaj nám

zanechať hriešne návyky a začať čnostný život, —
aby sme sa pripravili

na slávu nebeského kráľovstva.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Všemohúci Bože,

s radosťou prinášame dary na najsvätejšiu obetu; *
prosíme t'a, pomôž nám, —

aby sme ti ochotne obetovali aj svoje srdcia,
očistené telesným pokáním.

Skrze Krista, nášho Pána.

Pieseň vďaky o Umučení Pána 1. (str. 566).

SPEV NA PRIJÍMANIE

— Ak sa číta evanjelium o cudzoložnej žene: (Jn 8, IO-II)

124 PĎSTNE OBDOBIE

