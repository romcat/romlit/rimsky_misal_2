prosíme t'a, naplň aj nás vrúcnou láskou

k ukrižovanému Spasiteľovi, -

aby sme sa s ním mohli radovať,

keď sa zjaví vo svojej sláve.

O to t'a prosíme skrze nášho Pána Ježiša Krista,
tvojho Syna, ktorý je Boh a s tebou žije a kraľuje
v jednote s Duchom Svätým po všetky veky vekov.

25. júla
sv. ]AKUBA, APOŠTOLA
Sviatok
ÚVODNY SPEV (Porov. Mt 4, 18. 21)

Keď Ježiš išiel popri Galilejskom mori,

videl Jakuba, Zebedejovho syna, a jeho brata Jána,
ako si opravovali siete,

a povolal ich.

Oslavná pieseň.

MODLITBA DNA

Všemohúci a večný Bože,

ty si prvotnú činnosť apoštolov

požehnal mučeníckou krvou svätého Jakuba; *
vrúcne t'a prosíme, --

daj, aby tvoja Cirkev

čerpala posilu z jeho mučeníctva

a pod jeho ochranou sa ustavične vzmáhala.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

576 VLASTNÉ OMŠE SVÁTYCH

