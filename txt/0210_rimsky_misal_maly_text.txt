 

SOBOTA
po Tretej pôstnej nedeli

 

ÚVODNY SPEV (Ž 102, 2-3)

Dobroteč, duša moja, Pánovi
a nezabúdaj na jeho dobrodenia;
ved on ti odpúšťa všetky neprávosti.

MODLITBA DNA

Dobrotivý Otče,

s duchovnou radosťou konáme pokánie

v tejto pôstnej dobe; *

prosíme t'a, pomáhaj nám vždy hlbšie prežívať
veľkonočné tajomstvo, -—

aby sme v plnej miere dosiahli

jeho spásonosné účinky.

Skrze nášho Pána ]ežiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Svätý Bože, tebe vďačíme,

že môžeme pristupovať k tvojim svätým tajomstvám
s čistým srdcom; *

pomôž nám, —

aby sme ti slávením ich ustanovenia

vzdávali dôstojnú chválu.

Skrze Krista, nášho Pána.

Pieseň vďaky pôstna (str. 360 n.).

SPEV NA PRIJÍMANIE (Lk 18, 13)

Mýtnik stál celkom vzadu, bil sa do pŕs a hovoril:
Bože, buď milostivý mne hriešnemu.

110 POSTNE OBDOBIE

