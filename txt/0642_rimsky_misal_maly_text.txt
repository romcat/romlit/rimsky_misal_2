5. júna
SV. BONIFÁCA, BISKUPA A MUČENÍKA
Spomienka

Spoločná omša mučeníkov (str. 713 n. alebo 719)
alebo duchovných pastierov-misionárov (str. 738 n.)

MODLITBA DNA

Láskavý Bože, *

pomáhaj nám na orodovanie svätého

mučeníka Bonifáca, —

aby sme pevne vyznávali

a životom dosvedčovali vieru,

ktorú on neohrozene hlásal a spečatil svojou krvou.
Skrze nášho Pána ]ežiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

6. júna
SV. NORBERTA, BISKUPA

Spoločná omša duchovných pastierov-biskupov (str. 728 n.)
alebo svätých rehoľníkov (str. 759 n.).

MODLITBA DNA

Nekonečný Bože,

svätý Norbert sa pre svoju lásku k modlitbe
a pastiersku horlivosť

stal biskupom podľa tvojho srdca; *

na jeho príhovor daj svojmu ľudu

aj dnes biskupov a kňazov, —

542 VLASTNÉ OMŠE SVÁTYCH

