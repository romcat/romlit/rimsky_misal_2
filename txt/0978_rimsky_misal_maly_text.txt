PO PRIJÍMANÍ

Bože a Otče náš,

posväcuj nás týmto sviatostným pokrmom *
a daj, aby všetky národy

ochotne prijímali z rúk Cirkvi spásu, —
ktorú im tvoj Syn získal na kríži.

O to t'a prosíme skrze Krista, nášho Pána.

15. ZA PRENASLEDOVANYCH
KRESÍ'ANOV

ÚVODNý SPEV (Porov. Ž 73, 19-22)

Pamätaj, Bože, na svoje prisl'úbenia
a nikdy neopúšťaj svojich verných.
Povstaň, Pane, obráň svoje záujmy
a vypočuj prosby tých, čo sa utiekajú k tebe.

Alebo: (Sk 12, 5)

Petra strážili vo väzení.
Ale Cirkev sa bez prestania modlila k Bohu za neho.

MODLITBA DNA

Všemohúci a večný Bože,

vo svojej nevyspytateľnej prozreteľnosti

dávaš Cirkvi účasť na utrpení tvojho Syna; *
prosíme ťa, posilňuj svojich verných,

ktorí sú prenasledovaní pre vieru v teba, —

aby svojou trpezlivosťou a láskou

boli živým dôkazom pravdivosti tvojich prisľúbení.
Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

878 OMŠE ZA ROZLIČNÉ POTREBY

