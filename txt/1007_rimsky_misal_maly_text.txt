NAD OBETNYMI DARMI

Milosrdný Bože,

v tvojich rukách je každá chvíľka nášho života; *
prinášame ti obetné dary

za našich chorých bratov a sestry

(za nášho chorého brata M.) (za našu chorú sestru M.)
a prosíme ťa, —

aby sa naša úzkosť o ich (o jeho) (o jej) zdravie
obrátila na radosť

a mohli sme sa spolu s nimi (s ním) (s ňou) tešiť

z ich (z jeho) (z jej) uzdravenia.

Skrze Krista, nášho Pána.

SPEV NA PRIJÍMANIE (Kol 1, 24)

Na vlastnom tele dopĺňam,
čo ešte chýba Kristovmu umučeniu
pre dobro jeho tela, ktorým je Cirkev.

PO PRIJÍMANÍ

Bože, láskavý Otče,
ty si naše útočište najmä v dňoch choroby; *
pokorne prosíme tvoje milosrdenstvo,
ukáž svoju moc na našich chorých bratoch a sestrách
(na našom chorom bratovi M.)
(na našej chorej sestre M.)
a uzdrav ich (ho) (ju), —
aby sme sa mohli zasa s nimi (s ním) (s ňou) stretnúť
v našom farskom spoločenstve
a aby ti s nami mohli (mohol) (mohla) priniesť
obetu chvály.
Skrze Krista, nášho Pána.

ZA CHORYCH 907

