zmiluj sa nad nami, -

odpusť nám viny

a usmerňui k sebe naše nestále srdcia.
Skrze Krista, nášho Pána.

Pieseň vďaky nedeľná IV. (str. 377).

SPEV NA PRIJÍMANIE (Lk 15, m)

Pán Ježiš hovorí:
Boží anjeli sa budú radovať z jedného hriešnika,
čo robí pokánie.

PO PRIJÍMANÍ

Dobrotivý Otče,

pre túto sviatostnú obetu nám odpúšťaš viny; *
prosíme ťa, pomáhaj nám svojou milosťou
chrániť sa ďalších hriechov, —

aby sme ti slúžili s čistým srdcom.

Skrze Krista, nášho Pána.

41. ZA DAR LÁSKY

ÚVODNý SPEV (Porov. Ez 36, 26. 27. 28)

Pán hovorí: Odstránim z vášho tela srdce kamenné
a dám vám srdce iemnocitné.
Vložím do vás svoiho Ducha,
a budete mojím ľudom a ja budem vaším Bohom.

MODLITBA DNA

Všemohúci Bože, zošli nám Ducha Svätého *

a zapáľ naše srdcia ohňom svojej lásky, —

aby sme vždy zmýšľali a konali, ako sa tebe páči,

a úprimne ťa milovali v našich bratoch a sestrách.

918 OMŠE ZA ROZLIČNÉ POTREBY

