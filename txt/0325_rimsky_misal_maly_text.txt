— Rok B (Porov. Lk 24, 46-47)

Kristovi bolo treba trpieť a na tretí deň vstať z mŕtvych;
v jeho mene sa bude všetkým národom
ohlasovať pokánie na odpustenie hriechov. Aleluja.

— Rok C (Porov. Jn 2x, xz-rg)
Pán Ježiš povedal svojim učeníkom:

Poďte a jedzte.

Potom vzal chlieb a dával im. Aleluja.

PO PRIJÍMANÍ

Prosíme t'a, večný Bože, *

láskavo zhliadni na svoj ľud,

ktorý si duchovne obnovil
veľkonočnými sviatosťami, —

a prived' nás k slávnemu vzkrieseniu.
Skrze Krista, nášho Pána.

 

PONDELOK
po Tretej veľkonočnej nedeli

 

ÚVODNý SPEV

Vstal z mŕtvych dobrý Pastier.
On dal život za svoje ovce,
ochotne umrel za svoje stádo. Aleluja.

MODLITBA DNA

Milosrdný Bože,

ty ukazuješ blúdiacim svetlo svojej pravdy,
aby sa vrátili na správnu cestu; *

pomáhaj všetkým kresťanom

odmietať, čo sa protiví ich viere, —

a usilovať sa o to,

PONDELOK PO TRETE] NEDELI 225

