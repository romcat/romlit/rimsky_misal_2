PRVÁ PôSTNA'NEDEĽA

ÚVODNY SPEV (Porov. Ž 90, 15-16)

Keď ku mne zavolá, ja ho vyslyším,
zachránim ho, oslávim a obdarím dlhým životom.

Oslavná pieseň sa vynechá.

MODLITBA DNA

Všemohúci Bože,

udeľ nám milost', *

aby sme prežívaním štyridsaťdenného

pôstneho obdobia

hlbšie vnikali do Kristovho tajomstva —

a čnostným životom napredovali na ceste k spáse.
Skrze nášho Pána Ježiša Krista, tvojho Syna,
ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.
Vyznanie viery.

NAD OBETNYMI DARMI

Pane a Bože náš, pomôž nám, *

aby sme sa celým srdcom zapojili do tejto obety, —
ktorou začíname prípravu

na slávenie veľkonočného tajomstva.

Skrze Krista, nášho Pána.

PIESEN VĎAKY
O pokúšaní Pána

: Pán s vami.

: I s duchom tvojím.

: Hore srdcia.

: Máme ich u Pána.

: Vzdávajme vďaky Pánovi, Bohu nášmu.
: Je to dôstojné a správne.

PŠPŠPŠ

PRVÁ NEDEĽA 83

