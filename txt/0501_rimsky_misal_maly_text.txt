O ZOSNULYCH III
Kristus je spása a život

Táto pieseň vd'aky sa používa v omšiach za zosnulých.

: Pán s vami.
: I s duchom tvojím.
: Hore srdcia.

: Máme ich u Pána.
: Vzdávajme vďaky Pánovi, Bohu nášmu.
: Je to dôstojné a správne.

PŠPŠPŠ

Je naozaj dôstojné a správne, dobré a spásonosné
vzdávať vďaky vždy a všade

tebe, Pane, svätý Otče, všemohúci a večný Bože,
skrze nášho Pána Ježiša Krista.

Lebo on prináša svetu spásu,
dáva ľuďom nový život
a mŕtvych kriesi k večnému životu.

Skrze neho klaňajú sa ti zástupy anjelov
a večne sa radujú v tvojej prítomnosti.
Prosíme t'a, pripoj k nim aj naše hlasy,
keď spoločne voláme na tvoju slávu:

Svätý, svätý, svätý Pán Boh všetkých svetov...

O ZOSNULYCH IV

Z pozemského života do nebeskej slávy

Táto pieseň vďaky sa používa v omšiach za zosnulých.

V.: Pán s vami.
O.: I s duchom tvojím.

O ZOSNULYCH IV 401

