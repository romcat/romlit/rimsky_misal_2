príkladu Ježiša Krista, nášho Učiteľa a Pastiera, teda
nie z túžby po hmotných výhodách, ale jedine z
lásky k dušiam?

Všetci kňazi: Chcem.

Potom sa biskup obráti k ľudu a pokračuje:

A vy, milovaní bratia a sestry, modlite sa za svojich
kňazov: nech ich Pán zahrnie svojimi darmi, aby
vás ako verní pomocníci najvyššieho kňaza Ježiša
Krista viedli k nemu, k darcovi spásy.

Ľud: Kriste, uslyš nás. Kriste, vyslyš nás.

Modlíte sa aj za mňa, aby som verne plnil svoje
apoštolská poslanie a bol medzi vami čoraz pre-
svedčivejším a dokonalejším obrazom Ježiša Krista,
Veľkňaza, dobrého Pastiera, Učiteľa a Služobníka
Všetkých ľudí.

Ľud: Kriste, uslyš nás. Kriste, vyslyš nás.

Nech nás Pán zachová vo svojej láske a nech nás
všetkých, kňazov i l'ud, privedie do života večného.
Všetci: Amen=

Vyznanie viery sa vynechá, aj modlitba veriacich.

Liturgia Eucharistie

NAD OBETNYMI DARMI

Pane a Bože náš, *

nech nám sila tejto svätej obety

pomáha zanechať starý spôsob života, —
nech zveľaďuje v nás nový život

a zaistí nám večnú spásu.

Skrze Krista, nášho Pána.

150 VEĽKY TY'ŽDEN

