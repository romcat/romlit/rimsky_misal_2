15.

16.

17.

476

Kňaz: Nech vás žehná všemohúci Boh,
Otec i Syn % i Duch Svätý.
Ľud: Amen.

Kňaz: Najláskavejší Bože, posilňuj svoj l'ud,
aby odhodlane odmietal, čo sa ti nepáči,

a nachádzal radosť v plnení tvojej svätej vôle.
Skrze Krista, nášho Pána.

Ľud: Amen.

Kňaz: Nech vás žehná všemohúci Boh,
Otec i Syn % i Duch Svätý.
Ľud: Amen.

Kňaz: Všemohúci Bože,

vystri svoju pravicu na ochranu svojho ľudu,
ktorý t'a pokorne prosí;

očisťuj ho, ved' ho svojou pravdou

a dávaj mu všetko potrebné pre pozemský život,
aby mohol dosiahnuť dobrá večné.

Skrze Krista, nášho Pána.

Ľud: Amen.

Kňaz: Nech vás žehná všemohúci Boh,
Otec i Syn >? i Duch Svätý.
Ľud: Amen.

Kňaz: Prosíme t'a, Bože,
zhliadni na túto svoju rodinu,

za ktorú sa náš Pán Ježiš Kristus
neváhal vydat' do rúk zločincov
a podstúpiť muky kríža.

OMŠOVY PORIADOK

