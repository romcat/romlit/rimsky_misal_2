a z Ducha Svätého,
na všetok svoj ľud a na všetkých,
čo ťa hľadajú s úprimným srdcom.

SPEV NA PRIJÍMANIE (1 ]n 3, x)

Pozrite, akú veľkú lásku nám daroval Otec:
voláme sa Božími deťmi a nimi aj sme.

PO PRIJÍMANÍ

Dobrotivý Otče,

prijali sme sviatosť tela a krvi tvojho Syna; *
nech v nás tento nebeský pokrm upevňuje
spoločenstvo Ducha Svätého a bratskú lásku, —
aby sme mohli rásť k plnej miere

Kristovho tajomného tela.

O to t'a prosíme skrze Krista, nášho Pána.

B

ÚVODNY SPEV (Tit 3, 5. 7)
Boh nás spasil svojím veľkým milosrdenstvom,

očistným kúpeľom Ducha, ktorý obrodzuíe a obnovuje,

aby sme boli ospravedlnení jeho milosťou

a mali nádej i podiel na večnom živote.

MODLITBA DNA

Všemohúci Bože, *

svojím slovom života nás robíš novými ľuďmi; —-
daj, aby sme ho prijímali s úprimným srdcom,
podľa neho kráčali po ceste pravdy

a s láskou slúžili svojim bratom.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

PRI UDEĽOVANÍ KRSTU 781

