Pieseň vďaky () apoštoloch I. (str. 387).
SPEV NA PRIJÍMANIE (Mt 16, 16.18)

Peter povedal Ježišovi:

Ty si Kristus, Syn živého Boha.

]ežiš mu odpovedal:

Ty si Peter, a na tejto skale postavím svoju Cirkev.

PO PRIJÍMANÍ

Bože a Otče náš,

na sviatok svätého Petra apoštola

posilnil si nás telom a krvou svojho Syna; *
daj, prosíme, -

aby nám toto stretnutie s naším Vykupiteľom
bolo prameňom jednoty a pokoja.

Skrze Krista, nášho Pána.

23. februára
SV. POLYKARPA, BISKUPA A MUČENÍKA

Spomienka

Spoločná omša mučeníkov (str. 713 n.)
alebo duchovných pastierov-biskupov (str. 728 n.).

MODLITBA DNA

Bože, Pán všetkého tvorstva,

ty si pridružil svätého biskupa Polykarpa

k zástupu mučeníkov; *

na jeho príhovor aj nám daj silu

pit' z kalicha Kristovho utrpenia, —

aby sme mali účasť na sláve vzkriesenia.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

23 . FEBRUÁRA 505

