14. septembra
POVYŠENIE SVÁTÉHO KRÍŽA

Sviatok

ÚVODNY SPEV (Porov. Gal 6, 14)
Hľadajme slávu v kríži nášho Pána Ježiša Krista.

On je naša spása, náš život, naše vzkriesenie;

on nás oslobodil a spasíl.

Oslavná pieseň.

MODLITBA DNA

Láskavý Otče,

ty si poslal na svet svojho milovaného Syna,

aby smrťou na kríži spasil všetkých ľudí; *

daj, prosíme, —

aby sme na zemi stále lepšie poznávali

tajomstvo jeho vykupiteľskej smrti

a dosiahli účasť na jeho sláve V nebi.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

V jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Prosíme ťa, milosrdný Bože, *

nech nás očistí od každej viny

obeta Ježiša Krista, -—

ktorý na oltári kríža sňal hriechy celého sveta.
Lebo on žije a kraľuje na veky vekov.

PIESEN VĎAKY
O VíťaZStve slávneho kríža

V.: Pán s vami.
O.: 1 s duchom tvojím.

616 VLASTNÉ OMŠE SVÁTYCH

