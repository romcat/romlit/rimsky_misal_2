Štvrtá eucharistická modlitba .................. 435
Slávnostné požehnania ..................... 458
Modlitby nad ľudom ..................... 471

VLASTNÉ OMŠE NA SVIATKY svÁTýCH

Január ............................. 483
Február ............................ 494
Marec .................... . ........ 506
Apríl .............................. 5x4
Máj .............................. 525
Jún .............................. 539
Júl . .............................. 56!
August ............................. 585
September ........................... 6m
Október ............................ 632
November ........................... 649
December ............................ 670

SPOLOČNÉ OMŠE

Spoločné omše posvätenia chrámu ............... 688
Spoločné omše preblahoslavenei Panny Márie ......... 695
Spoločné omše mučeníkov ................... 706
Spoločné omše duchovných pastierov .............. 725
Spoločné omše učiteľov Cirkvi ................. 743
Spoločné omše panien ..................... 746
Spoločné omše svätých mužov a svätých žien .......... 752

OMŠE PRI VYSLUHOVANÍ SVIATOSTÍ A SVÁTENÍN

[. Pri sviatostiach uvádzania do kresťanského života:
1. Pri prijatí alebo zápise katechumenov ......... 773
2. Pri prípravných skúškach na krst ............ 774
3. Pri udeľovaní krstu .................. 778
4. Pri udeľovaní sviatosti birmovania ........... 782
II. Pri vysviacke diakona, kňaza a biskupa .......... 788
III. Pri vysluhovaní viatika .................. 790
IV. Za ženícha a nevestu
1. Pri uzatváraní manželstva ............... 791
2. Na výročia manželstva ................ 806

1048 OBSAH

