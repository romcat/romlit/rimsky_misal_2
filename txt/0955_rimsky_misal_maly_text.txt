a nech ich pobáda hľadať česť a slávu tvojho mena.
Skrze Krista, nášho Pána.

Omša za duchovné alebo pastoračné zhromaždenie, pozri č. 16, (str.
880).

6. ZA KNAZOV
ÚVODNY SPEV (Lk 4, 18. 19)

Duch Pánov je nado mnou, pretože ma pomazal,
aby som hlásal blahozvesť chudobným
a uzdravoval skrúšených srdcom.

MODLITBA DNA

Všemohúci Bože,

ty si ustanovil svojho jednorodeného Syna

za najvyššieho a večného kňaza; *

vypočuj našu modlitbu za tých,

ktorých on sám vyvolil za vysluhovatel'ov

a správcov svätých tajomstiev, —

aby verne konali službu, ktorú si im Zveril.
Skrze nášho Pána Ježiša Krista, tvojho Syna...

Alebo :

Pane a Bože náš,

ty spravuješ kresťanský l'ud

prostredníctvom svojich kňazov; *

pomáhaj im, aby verne plnili poslanie,

ku ktorému si ich povolal, —

a tak svojou kňaZSkou službou i životom
oslavovali ťa v tvojom Synovi Ježišovi Kristovi,
ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

ZA KNAZOV 855

