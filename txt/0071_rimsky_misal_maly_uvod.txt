32.9. Omše za rozličné potreby sú
trojaké:

a) omše spojené s vysluhovaním
sviatostí alebo svätenin (Missae
rituales);

b) omše za rozličné potreby,
ktoré sa berú pri niektorých okol-
nostiach, vyskytujúcich sa občas
alebo v určitých obdobiach;

c) omše votívne o tajomstvách
Pána, alebo na počesť preblahosla—
venej Panny Márie, alebo niekto-
rého svätého, alebo všetkých svä-
tých, ktoré sa môžu slobodne vy-
brať podľa nábožnosti veriacich.

330. Omše spojené s vysluhovaním
sviatostí alebo svätenín (Missae
rituales) sa zakazujú: v nedele ad—
ventné, pôstne, veľkonočné, ďalej
na slávnosti, cez veľkonočnú ok-
távu, v deň Spomienky na všetkých
verných zosnulých, na Popolcovú
stredu a vo všedné dni Veľkého
týždňa. Okrem toho treba zacho-
vať smernice, uvedené v rituáloch
alebo v samých omšiach.

33x. Z omší za rozličné potreby
môže príslušná cirkevná vrchnosť
vybrať omše pre prosebné pobož-
nosti v priebehu roku, ktoré určí
biskupská konferencia.

332. Ak sa vyskytne nejaká váž-
nejšia potreba alebo pastoračný
dôvod, priliehavú omšu možno
sláviť 2 nariadenia alebo povolenia
miestneho ordinára vo všetky dni,
okrem slávností, nedieľ advent-
ných, pôstnych a veľkonočných,
dalej okrem dní vo veľkonočnej
oktáve, okrem dňa Spomienky na
všetkých verných zosnulých, o-
krem Popolcovej stredy a všedných
dní Veľkého týždňa.

3 3 3. V dňoch, na ktoré pripadá
povinná spomienka, alebo vo všed-

ných dňoch adventných do 16. de-
cembra, vianočných od 2. januára
a veľkonočných po veľkonočnej
oktáve, omše za rozličné potreby
a omše votívne sa nedovoľujú. Ak
si to však vyžaduje opravdivá po-
treba alebo úžitok veriacich, pri
omši za účasti ľudu možno vziať
omšový formulár zodpovedajúci
tejto potrebe alebo úžitku veriacich.
Ponecháva sa to na úsudok správcu
kostola alebo samého celebranta.

334. V0 všedné dni << cez rok », na
ktoré prípadnú ľubovoľné spo-
mienky alebo v ktoré sa slávi litur-
gia hodín všedného dňa, možno
celebrovať akúkoľvek omšu alebo
použiť akúkoľvek modlitbu ad di-
versa okrem omší spojených s vy-
sluhovaním sviatosti a svätenín.

II. Omše za zosnulých

335. Eucharistickú obetu Kristovho
veľkonočného tajomstva Cirkev
obetuje za zosnulých, aby v spo-
ločenstve všetkých Kristových ú-
dov táto obeta jedným vyprosovala
duchovnú pomoc, iným zasa pri—
niesla potechu nádeje.

336. Medzi omšami za zosnulých
na prvom mieste stoji pohrebná
omša, ktorá sa môže sláviť každý
deň okrem prikázaných slávnosti,
okrem štvrtku vo Veľkom týždni,
okrem Veľkonočněho trojdnia, ďa-
lej okrem nedieľ adventných, pôst-
nych a veľkonočných.

337. Omšu za zosnulých po prijatí
zvesti o smrti, pri definitívnom po-
chovávaní zomrelého alebo v deň
prvého výročia možno sláviť aj
v dňoch vianočnej oktávy, ako aj
v dňoch, na ktoré pripadá povinná
spomienka alebo všedný deň litur-

VŠEOBECNÉ SMERNICE - MISÁL 7l*

