NAD OBETNYMI DARMI

Dožič nám, Bože,

prinášať ti s oddaným srdcom túto obetu, *
ktorá sprítomňuje eucharistické tajomstvo, —
a daj, aby V nás uskutočňovala dielo spásy.
Skrze Krista, nášho Pána.

Pieseň vďaky adventná I. (str. 354).

SPEV NA PRIJÍMANIE (Zjv 22, 12)

Pán hovorí: Hľa, prídem čoskoro a so mnou príde moja odplata;
odmením každého podľa jeho skutkov.

PO PRIJÍMANÍ

Prosíme t'a, milostivý Bože, *

dai nám týmto nebeským pokrmom silu
premáhat' naše zlé sklony, —

a tak sa pripraviť na blížiace sa sviatky.
Skrze Krista, nášho Pána.

SOBOTA PO PRVEJ NEDELI 13

