 

ŠTVRTOK

 

ÚVODNY SPEV (Porov. In I, 1)

Na počiatku a pred vekmi bolo Slovo,
a to Slovo bolo Boh;

v plnosti času sa nám narodil

ako Spasiteľ sveta.

MODLITBA DNA

Pred slávnostou Zjavenia Pána

Dobrotivý Otče,

narodením tvojho milovaného Syna

obdivuhodne si začal dielo našej spásy; *

prosíme t'a, posilňuj našu vieru, —

aby sme s jeho pomocou

dosiahli prisľúbená odmenu slávy.

Lebo on je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

Po slávnosti Zjavenia Pána

Večný Bože,

pri narodení tvojho Syna

všetkým národom zažiarila nádej na večný život; *
daj, aby sme stále lepšie poznávali

nášho Vykupiteľa —

a s jeho pomocou vošli do svetla večnej slávy.
Lebo on je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Dobrotivý Otče, prijmi tieto dary,
ktorými sa uskutočňuje vznešená výmena: *

ŠTVRTOK 63

