I. POHREBNÉ OMŠE

A. MIMO VEĽKONOČNÉHO OBDOBIA

ÚVODNY' SPEV (Porov. 4 Ezd 2, 34-35)

Odpočinutie večné daj zosnulým, Pane,
a svetlo večné nech im svieti.

MODLITBA DNA

Bože, všemohúci Otče,

veríme, že tvoj Syn umrel a vstal z mŕtvych; *
pre toto tajomstvo dožič milostivo, —

aby tvoj služobník M., (tvoja služobnica M.,)
ktorý(á) umrel(a) v spojení s Kristom,

v ňom dosiahol (dosiahla) aj vzkriesenie.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

Alebo :

Bože, plný milosrdenstva a zľutovania,

pokorne t'a prosíme

za tvojho služobníka M., (tvoju služobnicu M.,)
ktorého (ktorú) si (dnes) povolal k sebe; *

splň jeho (jej) nádej a vieru, —

uveď ho (ju) do nebeského domova

a daj mu (jej) večnú radosť.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

POHREBNÉ OMŠE 957

