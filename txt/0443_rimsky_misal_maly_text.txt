Kňaz: Ukáž nám, Pane, svoje milosrdenstvo.

Ľud: A daj nám svoju spásu.

Kňaz: Nech sa zmiluje nad nami všemohúci Boh,
nech nám hriechy odpustí a privedie nás do života
večného.

Ľud : Amen.

3 . formula

Tretia formula úkonu kajúcnosti nahrádza vzývanie Pána - Kýrie.

Kňaz: Bratia a sestry, uznajme svoje hriechy,
aby sme mohli s čistým srdcom sláviť sväté tajomstvá.

Chvíla ticha.

Kňaz: Pane Ježišu,

ty si bol poslaný uzdraviť skrúšených srdcom:
Pane, zmiluj sa.

Ľud: Pane, zmiluj sa.

Kňaz : Ty si prišiel volat' hriešnikov : Kriste, zmiluj sa.
Ľud: Kriste, zmiluj sa.

Kňaz: Ty sedíš po pravici Otca, aby si nás zastával:
Pane, zmiluj sa.
Ľud: Pane, zmiluj sa.

Kňaz: Nech sa zmiluje nad nami všemohúci Boh,
nech nám hriechy odpustí a privedie nás do života
večného.

Ľud: Amen.

Ďalšie predlohy ;. formuly úkonu kajúcnosti sú uvedené na str. 1012.

 

ÚVODNÉ OBRADY 343

