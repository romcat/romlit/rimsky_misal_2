PôSTNA 11

O duchovnom pokání

Táto pieseň vďaky sa používa v pôstnom období, najmä v nedeľu, ked
nie je predpísaná iná, priliehavejšia pieseň vďaky.

: Pán s vami.

: I s duchom tvojím.

: Hore srdcia.

: Máme ich u Pána.

: Vzdávajme vďaky Pánovi, Bohu nášmu.
: Je to dôstoiné a správne.

.OŠPŠPŠ

Je naozaj dôstojné a správne, dobré a spásonosné
vzdávať vďaky vždy a Všade

tebe, Pane, svätý Otče, všemohúci a večný Bože.
Lebo ty si nám určil tento posvätný čas

na duchovnú obnovu,

aby sme si očistili srdce od nezriadených žiadostí
a uprostred pozemských starostí '

mali vždy na zreteli večné hodnoty.

Preto t'a so všetkými anjelmi a svätými oslavujeme
a bez prestania voláme:

Svätý, svätý, svätý Pán Boh všetkých svetov...

PôSTNA III

0 ovocí zdržanlivosti

Táto pieseň vďaky sa používa vo všedné dni pôstneho obdobia a v dni
prikázaného pôstu.

V.: Pán s vami.
O.: I s duchom tvojím.

POSTNA III 361

