aby sme vydávali svedectvo

o jeho vykupiteľskom diele pred všetkými ľuďmi.
O to ťa prosíme skrze nášho Pána Ježiša Krista,
tvojho Syna, ktorý je Boh a s tebou žije a kraľuje
v jednote s Duchom Svätým po všetky veky vekov.

Obnovenie kňazských sľubov
Po homílii sa biskup prihovori takto alebo podobne:

Milovaní bratia, Kristus Pán dal apoštolom aj
nám účasť na svojom kňazstve. Dnes, keď opäť slá—
vime výročie tejto udalosti, ako váš biskup sa vás
pýtam:

Chcete predo mnou a pred Božím ľudom obnoviť
sľuby, ktoré ste kedysi urobili?
Všetci kňazi naraz odpovedia: Chcem.

Chcete sa bližšie primknúť k Pánu Ježišovi a lepšie
sa mu prispôsobiť?
Všetci kňazi: Chcem.

Chcete sa zrieknuť samých seba a verne plniť kňaz-
ské poslanie, ktoré ste z lásky ku Kristovi a jeho
Cirkvi vzali na seba v deň svojej kňazskej vysviacky?
Všetci kňazi: Chcem.

Chcete v Eucharistii a v ostatných liturgických úko-
noch verne vysluhovať Božie tajomstvá?
Všetci kňazi: Chcem.

Chcete posvätný učiteľský úrad vykonávať podľa

ZELENY ŠTVRTOK 149

