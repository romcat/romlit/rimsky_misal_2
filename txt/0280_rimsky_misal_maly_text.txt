8. Kňaz pozdraví zhromaždený l'ud zvyčajným spôsobom a krátko ho
poučí o veľkonočnej vigílii týmito alebo podobnými slovami:

Bratia a sestry, v túto presvätú noc náš Pán ježiš
Kristus vstal z mŕtvych. Preto Cirkev po celom svete
Zvoláva svojich synov a dcéry a zhromažďuje ich,
aby bedlili a modlili sa. Aj my sme prijali toto po-
zvanie.

Pripomenieme si Pánovo zmŕtvychvstanie, budeme
počúvať Božie slovo a oslávime veľkonočné tajomstvo
v nádeji, že budeme mať podiel na Kristovom ví-
ťazstve nad smrťou a na jeho živote v Bohu.

9. Potom požehná oheň.

Modlime sa.

Všemohúci a večný Bože,

skrze svojho Syna ožiaril si nás

svetlom svojej slávy; *

posvät' % tento nový oheň

a veľkonočnou slávnosťou

rozniet' v nás túžbu po tebe, _-

aby sme s čistým srdcom

mohli vojsť do kráľovstva večného svetla.
Skrze Krista, nášho Pána. O.: Amen.

Od posväteného ohňa sa zapáli veľkonočné svieca.

 

10. Ak je pre veriacich užitočnejšie zvýrazniť dôstojnosť a význam vel-
konočnej sviece nejakými symbolmi, možno to urobiť takto:

Po požehnani nového ohňa akolyta alebo jeden z prisluhujúcich pri-
nesie pred celebranta veľkonočnú sviecu. Celebrant vryje do sviece kríž;
nad kríž napíše grécke písmeno alfa, pod kríž písmeno omega a medzi
ramená kríža vpiše štyri číslice bežného roku. Pritom hovori:

180 VEĽKONOČNÁ VIGÍLIA

