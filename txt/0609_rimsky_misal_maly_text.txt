MODLITBA DNA

Nekonečný Bože,

prostredníctvom svätého biskupa Cyrila
priviedol si svoju Cirkev

k hlbšiemu chápaniu tajomstiev spásy; *

na jeho orodovanie nám pomáhaj, —

aby sme vždy lepšie poznávali tvojho Syna

a v ňom hľadali plnosť života.

Skrze nášho Pána Ježiša Krista, tvojho Syna...

19. marca

SVÁTÉHO JOZEFA
ŽENÍCHA PANNY MÁRIE

Slávnosť

ÚVODNY SPEV (Porov. Lk 12, 42)

Hľa, toto je verný a spravodlivý služobník,
ktorého Pán ustanovil nad svojou rodinou.

Oslavná pieseň .

MODLITBA DNA

Všemohúci Bože,

ty si svojho Syna a jeho panenskú matku

zveril vernej ochrane svätého Jozefa; *

daj, prosíme, —-

aby tvoja Cirkev, pod ochranou Pánovho pestúna,
ustavične pracovala na spáse všetkých ľudí.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po Všetky veky vekov.

19. MARCA 509

