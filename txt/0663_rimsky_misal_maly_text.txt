v Ježišovom mene.
Lebo on je Boh a s tebou žije a kraľuje
v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Milosrdný Bože,

pri oslave mučeníckej smrti svätého apoštola Tomáša
prinášame ti povinnú obetu chvály *

a pokorne t'a prosíme, —

zachovaj v nás milosť vykúpenia.

Skrze Krista, nášho Pána.

Pieseň vďaky o apoštoloch (str. 387 n.).

SPEV NA PRIJÍMANIE (Porov. ]n zo, 27)

Daj ruku a vlož ju do môjho boku.
a nebuď neveriaci, ale veriaci.

PO PRIJÍMANÍ

Milosrdný Bože,

v tejto sviatosti sme prijali

skutočné telo tvojho osláveného Syna *

a spolu s apoštolom Tomášom

v Kristovi spoznávame nášho Pána a Boha; —
daj, aby sme sa k nemu hlásili nielen slovom,
ale celým svojím životom.

Lebo on s tebou žije a kraľuje na veky vekov.

4. júla

SV. ALŽBETY PORTUGALSKE]

Spoločná omša svätých, čoílkonali skutky milosrdenstva (str. 762).

4. ]ÚLA 563

