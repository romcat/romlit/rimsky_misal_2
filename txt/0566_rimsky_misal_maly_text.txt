Kňaz: Nech vás žehná všemohúci Boh, Otec i Syn
% i Duch Svätý.
Ľud: Amen.

14. V OBDOBÍ << CEZ ROK » V

Kňaz: Nech všemohúci Boh odvráti od vás každé'

zlo a zahrnie vás svojím požehnaním.
Ľud: Amen.

Kňaz: Nech vzbudí vo vašich srdciach túžbu po

Božom slove a pripraví vás pre večnú radosť.
Ľud: Amen.

Kňaz: Nech vás poučí, čo je dobré a správne, a nech
vás povzbudí kráčať cestou svojich prikázaní, aby ste
dosiahli spoločenstvo so svätými v nebi.

Ľud: Amen.

Kňaz: Nech vás žehná všemohúci Boh, Otec i Syn
% i Duch Svätý.
Ľud: Amen.

PRI OSLAVE SVÁTYCH

15. NA SVIATKY PREBLAHOSLAVENE]
PANNY MARIE

Kňaz: Nech na vás zostúpi požehnanie všemohú-
ceho Boha Otca, ktorý nás vykúpil skrze Ježiša
Krista, narodeného z preblahoslavenei Panny Márie.
Ľud: Amen. '

466 OMŠOVY PORIADOK

