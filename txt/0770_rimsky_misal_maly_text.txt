 

DECEMBER

 

3. decembra
SV. FRANTIŠKA XAVERSKÉHO, KNAZA

Spomienka
(v Banskobystrickej diecéze sviatok hlavného patróna)

Spoločná omša duchovných pastierov-misionárov (str. 738 n.).

V Banskobystrickej diecéze Oslavná pieseň.

MODLITBA DNA

Bože, spása všetkých ľudí,

misionárskou činnosťou svätého

Františka Xaverského

získal si mnohé národy pre svoju Cirkev; *
vrúcne ťa prosíme, naplň srdcia veriacich
horlivosťou za šírenie viery, —

aby tvoja Cirkev po celom svete

rástla počtom a svätosťou.

Skrze nášho Pána Ježiša Krista, tvojho Syna...

NAD OBETNYMI DARMI

Bože, Pán všetkých národov, *

prijmi naše obetné dary

v deň spomienky na svätého Františka,
ktorý sa z túžby po spáse ľudí

odobral do ďalekých krajín ohlasovať
Kristovu blahozvest'; —

daj, aby sme aj my boli pravými svedkami evanjelia
a s našimi bratmi a sestrami vo viere
uberali sa k tebe.

Skrze Krista, nášho Pána.

670 VLASTNÉ OMŠE SVÁTYCH

