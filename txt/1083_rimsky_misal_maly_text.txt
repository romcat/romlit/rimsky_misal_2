ktorý za svojho života prinášal svätú obetu
za spásu tvojho l'udu; —

daj, aby teraz aj jemu bola prameňom
odpustenia a pokoja.

Skrze Krista, nášho Pána.

PO PRIJÍMANÍ

Láskavý Otče,

buď milosrdný svojmu služobníkovi,

nášmu zosnulému biskupovi M. *

a pre túto obetu prijmi ho

do večného spoločenstva s tvojím Synom, —
ktorého v tomto živote ohlasoval a v ktorého dúfal.
O to ťa prosíme skrze Krista, nášho Pána.

B. Za iného biskupa

MODLITBA DNA

Všemohúci Bože,

svojho služobníka, biskupa (kardinála) M.

si obdaril veľkňazskou hodnosťou

a ustanovil si ho za nástupcu apoštolov; *

vrúcne ťa prosíme, —

daj, aby sa v spoločenstve svätých apoštolov
večne radoval v tvojom kráľovstve.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

ZA BISKUPA 983

