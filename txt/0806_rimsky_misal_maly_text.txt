x_—
SPOLOČNÉ OMŠE MUČENÍKOV

 

I. Omša viacerých mučeníkov
mimo veľkonočného obdobia

ÚVODNÉ? SPEV

V nebi sa radujú svätí, ktorí nasledovali Krista,
a keďže z lásky k nemu vyliali svoju krv,
s ním požívajú večné šťastie.

MODLITBA DNA

Všemohúci Bože,

dnes si s úctou pripomíname

víťaznú smrť svätých mučeníkov M. a M.; *
prosíme ťa, —

vyslyš naše modlitby a daj nám silu,

aby sme nasledovali ich vytrvalosť vo viere..
Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Najsvätejší Otče, *

prijmi dary, ktoré ti prinášame

pri spomienke na svätých mučeníkov,

a pomáhaj aj nám, —

aby sme neochvejne vyznávali tvoje sväté meno.
Skrze Krista, nášho Pána.

706 SPOLOČNÉ OMŠE

