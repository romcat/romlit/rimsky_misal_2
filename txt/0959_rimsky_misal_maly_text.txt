SPEV NA PRIJÍMANIE (Porov. Lk 22, 28-30)

Pán Ježiš povedal tým, čo vytrvali s ním v skúškach:
]a vám pripravujem kráľovstvo,
aby ste mohli jesť a piť pri mojom stole.

PO PRIJÍMANÍ

Svätý Otče, posilnil si ma chlebom z neba

a kalichom Novej zmluvy si ma naplnil radosťou; *
pomáhaj mi, aby som ti verne slúžil -—

a celý svoj život venoval spáse ľudí.

Skrze Krista, nášho Pána.

€
Na výročie svojej vysviacky

ÚVODNY SPEV (Jn x;, 16)

Pán Ježiš hovorí: Nie vy ste si vyvolili mňa,
ale ja som si vyvolil vás a ustanovil som vás,
aby ste šli a prinášali ovocie a vaše ovocie aby zostalo.

MODLITBA DNA

Milosrdný Bože,

ty si ma bez mojich zásluh vyvolil,

aby som mal účasť na Kristovom večnom kňazstve
a konal službu v tvojej Cirkvi; *

daj, aby som bol horlivým

a láskavým hlásateľom evanjelia —

a pokorným správcom tvojich tajomstiev.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

KNAz ZA SEBA SAMÉHO 859

