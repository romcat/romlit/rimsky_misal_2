PO PRIJÍMANÍ

Všemohúci Bože, *

nech je nám táto vznešená sviatosť

výdatným duchovným pokrmom a nápojom, —-
aby sme sa premieňali V Ježiša Krista,

ktorého sme prijali.

O to t'a prosíme skrze Krista, nášho Pána.

 

DVADSIATA ôSMA NEDEĽA

 

ÚVODNY SPEV (Ž 129, 3-4)

Ak si budeš, Pane, v pamäti uchovávať neprávosť,
Pane, kto obstojí?

Ale ty si milostivý

a my ti chceme v bázni slúžiť.

Oslavná pieseň.

MODLITBA DNA

Prosíme t'a, všemohúci Bože, *

nech nás tvoja milosť

ustavične predchádza i sprevádza, —-

aby sme vždy horlivo konali dobré skutky.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

Vyznanie viery.

NAD OBETNYMI DARMI

Láskavý Bože *
prijmi naše modlitby a dary —

318 OBDOBIE << CEZ ROK »

