pomáhaj nám užívať tvoje dary

na chválu tvojho mena,

na udržanie nášho života

a v prospech spoločného dobra.

O to t'a prosíme skrze nášho Pána ]ežiša Krista,
tvojho Syna, ktorý je Boh a s tebou žije a kraľuje
v jednote s Duchom Svätým po Všetky veky vekov.

Alebo:

Láskavý Bože,

ďakujeme ti za úrodu našich polí

a za tvoju starostlivosť o nás; *

vrúcne t'a prosíme, -—

daj, aby aj v našich srdciach dozrelo semeno milosti
a prejavovalo sa skutkami lásky.

Skrze nášho Pána ]ežiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNýMI DARMI

Bože a Otče náš,

uštedril si nám zemské plody,

z ktorých ti s vďačným srdcom
prinášame na obetu chlieb a víno; *
posvät' ich a daj nám milosť, —

aby aj v našich srdciach dozrelo ovocie,
ktoré bude trvat“ naveky.

Skrze Krista, nášho Pána.

Pieseň vďaky nedeľná V. (str. 378).

SPEV NA PRIJÍMANIE (Ž 103, x;. 14. 15)

Pane, plodmi svojich diel sýtiš zem.
Zo zeme vyvádzaš chlieb i víno, čo obvesel'uje srdce človeka.

POĎAKOVANIE ZA ÚRODU 899

