SPEV NA PRIJÍMANIE (Ž 137, x)

Chcem ťa, Pane, oslavovať celým srdcom,
budem ti hrať pred tvárou anjelov.

PO PRIJÍMANÍ

Dobrotivý Bože, *

posilnení nebeským chlebom pokorne ťa prosíme, —
aby nám tento sviatostný pokrm pomáhal
odhodlane napredovať na ceste spásy

pod mocnou ochranou tvojich anjelov.

Skrze Krista, nášho Pána.

30. septembra

SV. HIERONYMA, KNAZA A UČITEĽA CIRKVI

Spomienka

ÚVODNý SPEV (]oz 1, x8)

Nech sa nevzdaluje kniha Zákona od tvojich pier;
rozjímaj o nej vo dne i v noci,

aby si zachoval a konal všetko, čo je v nej napísané.
Vtedy budeš najlepšie usmerňovať svoju cestu

a budeš mať úspech.

MODLITBA DNA

Všemohúci Bože,

svätého kňaza Hieronyma si obdaril láskou

a živým záujmom o Sväté písmo; *

otvor aj naše srdce pre tvoje slovo, -—

aby sme sa ním živili

a v ňom našli prameň života.

Skrze nášho Pána Ježiša Krista, tvojho Syna...

630 VLASTNÉ OMŠE SVÁTYCH

