.: (Pán s vami.

: I s duchom tvojím.)

: Hore srdcia.

: Máme ich u Pána.

.: Vzdávajme vďaky Pánovi, Bohu nášmu.
: Je to dôstojné a správne.

.O<.O.<.O<

]e naozaj dôstojné a správne

z hĺbky srdca a z celej duše zvelebovat
neviditeľného, všemohúceho Boha Otca
a ospevovat' jeho jednorodeného Syna,
nášho Pána Ježiša Krista.

On namiesto nás splatil večnému Otcovi
dlžobu za hriech Adamov

a svojou krvou zrušil výrok odsúdenia
za prvotnú vinu.

Lebo slávime veľkonočné sviatky,
keď bol zabitý pravý Baránok,
ktorého krv posväcuje dveraje veriacich.

Toto je noc,

v ktorej si našim otcom, synom Izraela,
pomohol prejsť suchou nohou cez Červené more,
keď si ich vyviedol z Egypta.

Toto je noc,
v ktorej jas ohnivého stlpa
rozohnal temnoty hriechu.

Toto je noc,
ktorá dnes na celej zemi

veriacich v Krista
vymaňuje z neprávosti sveta

184 VEĽKONOČNÁ VIGÍLIA

