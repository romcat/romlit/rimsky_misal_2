s tvojím služobníkom, naším pá-
pežom M., s naším biskupom (vi—
károm, prelátom, prefektom, opá-
tom) M.

V eucharistickej modlitbe možno
menovať aj pomocných a svätia-
cich biskupov. Ak treba menovať
viacerých, povie sa všeobecnou
formulou: s naším biskupom M.
a s jeho pomocnými biskupmi.Go
V každej eucharistickej modlitbe
treba uvedené formuly prispôsobiť
gramatickým pravidlám.

Krátko pred premenením poslu-
hujúci podľa potreby zvončekom
upozorní veriacich. Podľa miest-
nych zvykov možno zazvonit' pri
obidvoch pozdvihovaniach.

110. Po doxológii na konci eucha-
ristickej modlitby kňaz so zo-
pnutými rukami vyzve veriacich,
aby sa pomodlili modlitbu Pána,
a potom sa ju s rozopnutými ru-
kami modlí spolu s l'udom.

111. Po modlitbe Pána kňaz s ro-
zopnutými rukami sám hovorí em-
bolizmus Prosíme t'a, Otče, zbav
nás. Ľud ho zakončí zvolaním:
Lebo tvoje je kráľovstvo.

112. Potom sa kňaz nahlas modlí
Pane Ježišu Kriste, ty si povedal.
Po skončení rozopne a znova zopne
ruky a ohlasuje pritom pokoj slo-
vami Pokoj Pánov nech je vždy
s vami. Lud odpovie I s duchom
tvojim.Potom podl'a okolností kňaz
pridá Dajte si znak pokoja.A všetci
si podľa miestnych zvykov prejavia
navzájom pokoj a lásku. Kňaz mô-
že dat' znak pokoja posluhujúcim.

113. Potom kňaz vezme hostiu,
láme ju nad pate'nou, čiastočku
vpustí do kalicha a potichu hovorí

Telo a krv nášho Pána. Medzitým
zbor a lud spieva alebo hovorí
Baránok Boží (porov. č. 56 e).

114. Teraz sa kňaz potichu modlí
modlitbu Pane Ježišu Kriste, Syn
Boha živého alebo Pane ]ežišu
Kriste, nech mi prijatie.

115. Po tejto modlitbe kňaz po—
klakne, vezme hostiu, drží ju tro-
chu zdvihnutú nad pate'nou a obrá-
tený k ludu hovorí Hľa, Baránok

Boží a spolu s ľudom povie raz
Pane, nie som hoden.

116. Potom kňaz obrátený k oltáru
hovorí potichu Telo Kristovo nech
ma zachová pre život večný a pri-
jíma s úctou Kristovo telo. Potom
vezme kalich a hovorí Krv Kristova
nech ma zachová pre život večný
a s úctou prijíma Kristovu krv.

117. Teraz vezme pate'nu alebo ci-
bórium a pristúpi k prijímajúcim.
Ak sa prijímanie dáva iba pod
spôsobom chleba, každému ukáže
trochu zdvihnutú hostiu so slovami
Telo Kristovo. Prijímajúci odpovie
Amen a, držiac paténu pod ústami,
prijíma Sviatosť.

118. Pri prijímaní pod obidvoma
spôsobmi sa má zachovat obrad
opísaný na príslušnom mieste (po-
rov. č. 240-252).

119. Medzitým čo kňaz prijíma
Sviatosť, začína sa spev na pri-
jímanie (porov. č. 56 i).

120. Po rozdaní prijímania kňaz sa
vráti k oltáru, pozbiera odrobinky,
ak nejaké sú, potom na boku oltára
alebo na bočnom stolíku očistí
paténu alebo cibórium nad kali-
chom. Potom purifikuje kalich a

5" Porov. Posv. kongr. pre bohoslužbu, Decr. 9. októbra X972: AAS 64 (1971) str. 691-694.

VŠEOBECNÉ SMERNICE - MISÁL 43*

