Kiež stále krotím svoju prirodzenosť,
rozmnožujem milosť,
zachovávam zákon a zaslúžim si spásu.

Kiež sa naučím od teba, aké nepatrné je to, čo je pozemské,
aké veľké je to, čo je božské,

aké krátke je to, čo je časné,

aké trvalé to, čo je večné.

Daj, aby som sa pripravoval na smrť,
bál sa súdu,

vyhol zatrateniu a získal si nebo.
Skrze Krista, nášho Pána.

Amen.

MODLITBA K BLAHOSLAVENE] PANNE MÁRII

Mária, najsvätejšia Panna a Matka,

pozri, prijal som tvojho milovaného Syna,

ktorého si počala vo svojom nepoškvrnenom živote,
porodila, chovala a láskavo objímala.

Tešila si sa z pohľadu na neho

a prekypovala si všetkými radosťami.

Hla, jeho samého pokorne a s láskou prinášam tebe a obetujem,
aby si ho objala vo svojom náručí, poláskala vo svojom srdci
a obetovala Najsvätejšej Trojici

na jej najvyššiu poklonu,

na tvoju vlastnú česť a slávu

a na pomoc mne a celému svetu.

Preto t'a, dobrotivá Matka, prosím,

vypros mi odpustenie všetkých hriechov

a hojnosť milostí,

aby som mu odteraz vernejšie slúžil.

Napokon mi vypros milosť vytrvalosti,

aby som ho mohol s tebou oslavovať po všetky veky vekov.
Amen.

1032 DODATOK

