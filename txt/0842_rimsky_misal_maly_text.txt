Alebo (o mučeníkoch) :

Prosíme t'a, všemohúci Bože, *

daj, aby sme s oddanou láskou

nasledovali vieru svätého M. a M., -

ktorí za šírenie evanjelia položili život

a zaslúžili si veniec nehynúcej slávy.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNýMI DARMI

Dobrotivý Bože,

požehnaj obetné dary,

ktoré ti prinášame pri oslave svätého M., *

a premeň ich na sviatosť našej spásy, —

aby nás pri svätom prijímaní

očistili od každej viny

a pomáhali nám dosiahnuť večný život v nebi.
Skrze Krista, nášho Pána.

SPEV NA PRIJÍMANIE (Porov. Lk xo, x. 9)

Pán poslal učeníkov, aby všade ohlasovali;
Priblížilo sa k vám Božie kráľovstvo.

PO PRIJÍMANÍ

Večný Bože, *

zveľaďuj náš duchovný život sviatosťou,

ktorú sme prijali v deň pamiatky na svätého M., —
a daj, aby nás príklad jeho apoštolských čností
povzbudil k dôslednému životu podl'a evanjelia.
Skrze Krista, nášho Pána.

742 SPOLOČNÉ OMŠE

