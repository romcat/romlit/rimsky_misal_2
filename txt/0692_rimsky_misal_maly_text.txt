ktorý svojimi kázňami priviedol Cirkev k rozkvetu; *
daj, prosíme, —
aby sa Cirkev na jeho príhovor aj dnes vzmáhala

a dosiahla plný účinok tejto sviatosti.
Skrze Krista, nášho Pána.

IO. augusta

SV. VAVRINCA, DIAKONA A MUČENÍKA

Sviatok

ÚVODNY SPEV

Oslavujeme svätého diakona Vavrinca, _
ktorý ako služobník Cirkvi venoval svoj život chudobným;
získal si korunu mučeníctva a vošiel do radosti svojho Pána.

Oslavná pieseň.

MODLITBA DNA

Milosrdný Bože,

ohnivá láska k tebe

dala svätému diakonovi

Vavrincovi silu

verne slúžiť tebe a chudobným

a bez strachu podstúpiť mučenícku smrť; *
pokorne t'a prosíme, —

daj, aby sme t'a podľa jeho príkladu vrúcne milovali
a obetavo slúžili blížnym.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

592 VLASTNÉ OMŠE SVÁTY'CH

