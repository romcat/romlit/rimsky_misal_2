OMŠA V DEN SLÁVNOSTI

ÚVODNY SPEV (Múd 1, 7)

Duch Boží napĺňa celý svet;
on všetko udržiava
a pozná každú reč. Aleluja.

Alebo: (Porov. Rirn 5, 5; 8, 11)
V našich srdciach sa rozlieva Božia láska
skrze Ducha Svätého, ktorý je nám daný. Aleluja.

oslavná pieseň.

MODLITBA DNA

Všemohúci Bože,

tajomstvom dnešnej slávnosti

posväcuješ svoju Cirkev

vo všetkých krajinách a národoch; *

naplň celý svet darmi Ducha Svätého

a v srdciach svojich veriacich

obnovuj aj teraz veľké skutky svojej lásky, —
ktoré si konal pri prvom hlásaní evanjelia.
Skrze nášho Pána Ježiša Krista, tvojho Syna,
ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

Vyznanie viery.

NAD OBETNYMI DARMI

Prosíme t'a, nebeský Otče, *

daj, aby nám Duch Svätý pomáhal

stále lepšie chápať tajomstvo tejto obety —
a priviedol nás k plnému poznaniu pravdy
podľa prisľúbenia Ježiša Krista,

ktorý s tebou žije a kraľuje na veky vekov.

ZOSLANIE DUCHA SVÁTÉHO 275

