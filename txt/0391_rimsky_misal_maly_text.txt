Oslavná pieseň .

MODLITBA DNA

Všemohúci Bože,

tvoja prozreteľnost' je neomylná

vo všetkých rozhodnutiach; *

preto ťa prosíme, _

odvráť od nás, čo je škodlivé,

a daj, aby nám Všetko slúžilo na spásu.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

Vyznanie viery.

NAD OBETNYMI DARMI

Láskavý Bože,

s dôverou v tvoju dobrotivosť
prichádzame s darmi k svätému oltáru; *
daj, aby nás očistila tvoja milosť -—

a posvätila obeta, ktorú konáme.

Skrze Krista, nášho Pána.

Pieseň vďaky nedeľná (str. 374 n.).

SPEV NA PRIJÍMANIE (Z 16, 6)

K tebe, Bože, volám, lebo ty ma vyslyšíš.
Nakloň ku mne sluch a Vypočuj moje slová.

Alebo: (Porov. Mk u, 2.3.24)

Pán Ježiš povedal:
Veru, hovorím vám, verte, že dostanete všetko,
o čo v modlitbe prosíte, a splní sa vám to.

DEVIATA NEDEĽA 291

