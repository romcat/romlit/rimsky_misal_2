H1

H1

vzal kalich s vínom,
vzdával vďaky
a dal ho svojim učeníkom, hovoriac:

trocha sa skloní

VEZMITE
A PITE z NEHO VŠETCI:
TOTO JE KALICH MOJEJ KRVI,
KTORÁ SA VYLIEVA ZA VÁS
1 ZA VŠETKYCH
NA ODPUSTENIE HRIECHOV.
JE TO KRV
NOVEJ A VEČNEJ ZMLUVY.
TOTO ROBTE NA MOJU PAMIATKU.

Ukáže kalich ľudu, znova ho položí na korporál a pokľaknutím adoruje.

Potom povie:

Hľa, tajomstvo viery.
Ľud odpovie zvolaním:
Smrť tvoju, Pane, zvestujeme

a tvoje zmŕtvychvstanie vyznávame,
kým neprídeš v sláve.

 

Druhá formula :

Vyznajme tajomstvo viery.

Pane, keď jeme tento chlieb
a pijeme z tohto kalicha,

ŠTVRTÁ EUCHARISTICKÁ MODLITBA 441

