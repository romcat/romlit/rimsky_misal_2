z bohatstva svojej dobroty, —

aby sme ti cez celý život

slúžili s oddaným srdcom.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

22. novembra
SV. CECÍLIE, PANNY A MUČENICE

Spomienka

Spoločná omša mučeníkov (str. 713 11.)
alebo panien (str. 746 n.).

MODLITBA DNA

Dobrotivý Bože,

milostivo prijmi naše modlitby *

a na príhovor svätej Cecílie, panny a mučenice,
udeľ nám milosť, —

aby sme ťa s radostnou oddanosťou

mohli chváliť celým svojím životom.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

23. novembra

sv. KLEMENTA I., PÁPEŽA A MUČENÍKA

Spoločná omša mučeníkov (str. 713 n.)
alebo duchovných pastierov-pápežov (str. 725 n.).

666 VLASTNÉ OMŠE SVÁTYCH

