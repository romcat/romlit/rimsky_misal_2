niet žalmistu — aj žalm medzi
čitaniami.

Lektor má určenú úlohu pri slá-
vení Eucharistie a plní ju aj vtedy,
keď sú prítomní posluhujúci vyš-
šieho stupňa.

Aby sa u veriacich pri počúvaní
posvätných čítaní vznietila v srdci
láska k Svätému písmu a živý záu-
jem oň,53 treba, aby tí, čo sú po-
verení čítať Sväté písmo, boli na-
ozaj schopní a starostlivo pripra-
vení, aj keď nie sú ustanovení za
lektorov.

67. Žalmistovi prislúcha úloha spie—
vat' žalm alebo iný biblický spev
medzi čítaniami. Aby žalmista svo-
ju úlohu mohol riadne zastať, má
vediet' spievať žalmy a mat' aj
dobrý prednes a výslovnosť.

68. Z ostatných posluhujúcich nie-
ktori vykonávajú službu v pres-
bytériu, iní mimo presbytéria.

K prvým patria tí, ktorým bola
zverená úloha vysluhovať v mimo-
riadnych prípadoch sväté prijíma-
nie,54 a ďalej tí, čo nesú misál,
kríž, sviece, chlieb, víno, vodu,
kadidlo.

K druhým patria :

a) Komentátor, ktorý veriacim
dáva vysvetlenie a úpravy, aby ich
uviedol do bohoslužby a aby ju
lepšie porozumeli. Úpravy komen-
tátora majú byť dôkladne pripra-
vené, stručné a vecné. Pri plnení
svojej úlohy komentátor stojí na
vhodnom mieste pred veriacimi;
nemal by stát' pri ambone.

b) Uvádzači: v niektorých kra-
joch vítajú veriacich pri bráne kos-
tola a uvádzajú ich na príslušné
miesta. Okrem toho sa starajú o
poriadok pri liturgických sprie-
vodoch.

c) Zberatelia: v kostole robia
zbierky na cirkevné ciele.

69. je vhodné, aby najmä vo väč-
ších kostoloch a spoločenstvách
bol určený usporiadateľ. Má sa
starať () riadny priebeh bohoslu-
žieb a o to, aby posluhujúci konali
svoju službu dôstojne, presne a
nábožne.

70. Všetky služby, nižšie od dia-
konskej služby, môžu konat' muži-
-laici, hoci neboli na to úradne
ustanovení. Služby, ktoré sa kona-
jú mimo presbytéria, možno zverít'
aj ženám, ak to uzná za vhodné
správca kostola.

Biskupská konferencia môže po-
volit', aby čítania pred evanjeliom
a úmysly v modlitbe veriacich
predniesla na to súca žena, a pres-
nejšie určiť miesto, odkiaľ má
ohlasovať Božie slovo v liturgic—
kom zhromaždení.55

71. Kde je viacero posluhujúcich,
ktorí môžu konať tú istú službu,
môžu si medzi sebou podeliť jed-
notlivé časti tej istej služby. Napr.
jeden diakon si môže vziať spevné
časti, druhý diakon zasa službu
pri oltári. Ak sú viaceré čítania,
je dobré rozdeliť ich medzi viace-
rých lektorov. To isté platí aj o
iných úlohách.

““ Porov. Druhý varík. koncil, konšt. o posv. liturgii Xarromnrtum Cnnrr'h'um, č. 24.
5' Porov. Posv. kongr. pre sviatosti, inštr. Immmrae tarilalír 19. januára i973. č. :: AAS 6; (1973)

str. 265-166.

“5 Porov. Posv. kongr. pre bohoslužbu, inštr. Liturgicae ímlauralr'onn. ;. sept. X970. č. 7: AAS 62

(1970) str. 7oo-7ol .

38* VŠEOBECNÉ-SMERNICE - MISÁL

