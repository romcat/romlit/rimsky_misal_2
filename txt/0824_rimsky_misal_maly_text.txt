PO PRIJÍMANÍ

Láskavý Otče,

sviatosť, ktorú sme prijali,

a oslava svätej M.

sú nám zdrojom ustavičnej radosti; *
pokorne ťa prosíme, —

daj, aby sme horlivejšie žili v milosti,
ktorú si v nás zveľadil

pri slávení tejto obety.

Skrze Krista, nášho Pána.

724 SPOLOČNÉ OMŠE

