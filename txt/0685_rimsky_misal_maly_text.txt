 

AUGUST

1 . augusta

SV. ALFONZA MÁRIE DE LIGUORI,
BISKUPA A UČITEĽA CIRKVI

Spomienka

Spoločná omša duchovných pastierov—biskupov (str. 728 11.)
alebo učiteľov Cirkvi (str. 745 n.).

MODLITBA DNA

Dobrotivý Bože,

svojej Cirkvi stále udeľuješ nové vzory čností; *
daj, aby sme v horlivosti za spásu duší

kráčali po stopách svätého biskupa Alfonza -

a dosiahli s ním večnú odmenu.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Láskavý Bože,

svätému Alfonzovi si dal milosť

sláviť tieto tajomstvá

a v nich ti prinášať aj seba samého

ako svätú obetu; *

prosíme ťa, —

ohňom svojho Ducha očist' a posväť i naše srdcia.
Skrze Krista, nášho Pána.

1. AUGUSTA 585

