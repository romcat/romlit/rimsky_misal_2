Skrze nášho Pána Ježiša Krista, tvojho Syna,
ktorý je Boh a s tebou žije a kraľuje
v jednote s Duchom Svätým po všetky veky vekov.

23. decembra

SV. ]ÁNA KENTSKÉHO, KNAZA

Spoločná omša duchovných pastierov (str. 731 11.)
alebo svätých, čo konali skutky milosrdenstva (str. 762 n.).

MODLITBA DNA

Dobrotivý Bože, *

nech nám príklad svätého ]ána Kentského pomáha
rást' v múdrosti svätých, -

aby sme preukazovali milosrdenstvo všetkým,

a tak dosiahli tvoje odpustenie.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

26. decembra

sv. ŠTEFANA, PRVÉHO MUČENÍKA

Sviatok

ÚVODNY SPEV

Svätému Štefanovi sa otvorili brány neba.
Ako prvý z veľkého počtu mučeníkov
dosiahol veniec slávy.

Oslavná pieseň.

680 VLASTNÉ OMŠE SVÁTYCH

