V ROKU A

% Čítanie zo svätého evanjelia podľa Matúša.
(21, 1-11)

Keď sa priblížili k Jeruzalemu a došli do Betfage
pri Olivovej hore, Ježiš poslal dvoch učeníkov a po-
vedal im: << Choďte do dedinky oproti vám a hneď
nájdete priviazanú oslicu a pri nej osliatko. Odviažte
ich a priveďte mi ich. A keby vám niekto niečo
vravel, povedzte: “ Pán ich potrebuje." A hneď
ich prepustí. »

Toto sa stalo, aby sa splnilo slovo proroka: << Po-
vedzte dcére sionskej: Hľa, tvoj Kráľ prichádza
k tebe, ponížený, sediaci na oslici a na osliatku,
mláďati ťažného zvieraťa. »

Učeníci šli a urobili, ako im ]ežiš rozkázal. Pri-
viedli oslicu a osliatko, pokládli na ne svoje plášte
a on si na ne sadol. Mnohí zo zástupu prestierali
na cestu svoje plášte, iní stínali ratolesti zo stromov
a stlali ich po ceste. A zástupy, čo šli pred ním, i
tie, čo šli za ním, vyvolávali:

<< Hosanna Synovi Dávidovmu! Požehnaný, ktorý
prichádza v mene Pánovom. Hosanna na Výsos-
tiach! »

Keď ]ežiš vošiel do Jeruzalema, rozvírilo sa celé
mesto; vypytovali sa: << Kto je to? » A ľudia hovo-
rili: << Je to ]ežiš, prorok z galilejského Nazareta. »

Počuli sme slovo Pánovo.

134 VEĽKY TYŽDEN

