NAD OBETNYMI DARMI

Pane a Bože náš,

slávime pamiatku nesmiernej lásky tvojho Syna *
a pokorne t'a prosíme, —

aby prostredníctvom Cirkvi

ovocie jeho spasiteľného diela

bolo na spásu celému svetu.

Skrze Krista, nášho Pána.

Pieseň vďaky nedeľná VIII. (str. 380).

SPEV NA PRIJÍMANIE (Zjv ;, zo)

Hľa, stojím pri dverách a klopem, hovorí Pán.
Kto počúvne môj hlas a otvorí dvere,
vojdem k nemu a budeme spolu stolovať.

PO PRIJÍMANÍ

Všemohúci Bože,

ty neprestajne živíš Cirkev

svojím slovom a chlebom života; *

sprevádzaj nás svojou ochranou, —

aby v našom cirkevnom spoločenstve stále prekvitala
neporušená viera a svätosť života,

bratská láska a pravá nábožnosť.

Skrze Krista, nášho Pána.

Modlitbu nad obetnými darmi a modlitbu po prijímaní možno si vybrať
aj z predchádzajúcich omšových formulárov.

846 OMŠE ZA ROZLIČNÉ POTREBY

