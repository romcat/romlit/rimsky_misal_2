prosíme ťa, udeľ nám milosť, —

aby sme ti v každom stave a povolaní

verne slúžili, žili v tvojej prítomnosti

a dali sa viesť svetlom tvojej pravdy.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

17. marca
SV. PATRIKA, BISKUPA

Spoločná omša duchovných pastierov-misionárov (str. 738 n.)
alebo biskupov (str. 728 n.).

MODLITBA DNA

Večný Bože,

ty si poslal svätého biskupa Patrika

írskemu národu hlásať tvoju blahozvest'; *

pre jeho zásluhy a na jeho orodovanie

vzbuď misionárskeho ducha

vo všetkých kresťanoch, —-

aby ohlasovali svetu obdivuhodné dielo tvojej lásky.
Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

18. marca

SV. CYRILA ]ERUZALEMSKÉHO,
BISKUPA A UČITEĽA CIRKVI

Spoločná omša duchovných pastierov-biskupov (str. 728 n.)
alebo učiteľov Cirkvi (str. 743 n.).

508 VLASTNÉ OMŠE SVÁTYCH

