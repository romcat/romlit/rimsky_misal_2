SPEV NA PRIJÍMANIE (Jn 7. 37-38)

Pán Ježiš hovorí: Ak je niekto smädný,
nech príde ku mne a nech pije.
A z vnútra toho, kto verí vo mňa, potečú prúdy živej vody.

Alebo: (Jn 19, 34)

Jeden z vojakov prebodol kopijou ]ežišovi bok,
a hned vyšla krv a voda.

PO PRIJÍMANÍ

Milovaný Otče,

prijali sme sviatosť, ktorá je darom tvojej lásky; *
roznecui v nás lásku k tvojmu Synovi, —

aby sme ho vytrvalo nasledovali

a vedeli ho spoznať aj v našich bratoch.

Skrze Krista, nášho Pána.

NAJSV. SRDCA ]EŽIŠOVHO 335

