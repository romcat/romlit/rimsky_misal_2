NAD OBETNYMI DARMI

Dobrotivý Bože,

posväť tieto obetné dary svojím Duchom *
a skrze ne udeľ Cirkvi pravú lásku, -
aby celému svetu horlivo ohlasovala spásu,
ku ktorej povolávaš všetky národy.

Sere Krista, nášho Pána.

Pieseň vďaky svätodušné ako v nasleduiúceí omši.
Ak sa berie Rímsky kánon, použije sa vlastná vložka V spoločenstve.

SPEV NA PRIJÍMANIE (In 7, 37)

V posledný, veľký deň sviatku
]ežiš vstal a zvolal: Ak je niekto smädný,
nech príde ku mne a nech pije. Aleluia.

PO PRIJÍMANÍ

Láskavý Bože, *

nech nám prijaté sviatostné dary pomáhajú, —
aby sme ustavične planuli tým Duchom Svätým,
ktorým si zázračne naplnil svojich apoštolov.
Skrze Krista, nášho Pána.

274 VEĽKONOČNÉ OBDOBIE

