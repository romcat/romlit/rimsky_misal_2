On ma bez mojich zásluh, iba z milosti

pridružil k služobníkom oltára;

nech ma teda naplní jasom svojho svetla,

aby som mohol zaspievať chválospev o tejto svieci.)

V.: (Pán s vami.

O.: 1 s duchom tvojím.)

V.: Hore srdcia.

O.: Máme ich u Pána.

V.: Vzdávajme vďaky Pánovi, Bohu nášmu.
O.: Je to dôstojné a správne.

je naozaj dôstojné a správne

z hĺbky srdca a z celej duše zvelebovat
neviditeľného, všemohúceho Boha Otca
a ospevovat' jeho jednorodeného Syna,
nášho Pána Ježiša Krista.

On namiesto nás splatil večnému Otcovi
dlžobu za hriech Adamov

a svojou krvou zrušil výrok odsúdenia
za prvotnú vinu.

Lebo slávime veľkonočné sviatky,
keď bol Zabitý pravý Baránok,
ktorého krv posväcuje dveraje veriacich.

Toto je noc,

v ktorej si našim otcom, synom Izraela,
pomohol prejsť suchou nohou cez Červené more,
keď si ich vyviedol z Egypta.

Toto je noc,
v ktorej jas ohnivého stlpa
rozohnal temnoty hriechu.

VEĽKONOČNÁ VIGÍLIA 187

