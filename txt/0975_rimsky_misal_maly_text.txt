A

ÚVODNY SPEV (ž 66, 2-3)

Bože, buď nám milostivý a žehnaj nás,
a tvoja tvár nech žiari nad nami,

aby sa tvoja cesta stala známou na zemi
a tvoja spása medzi všetkými národmi.

MODLITBA DNA

Všemohúci Bože,

ty chceš spasiť všetkých ľudí

a priviesť ich k poznaniu pravdy; *

prosíme t'a, pozri na svoju veľkú žatvu

a pošli do nej robotníkov,

aby hlásali evanjelium všetkému stvoreniu; —
zhromažďuj svoj l'ud slovom života

a oživuj ho silou sviatostí,

aby kráčal po ceste spásy a lásky.

Skrze nášho Pána ]ežiša Krista, tvojho Syna,
ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

Alebo :

Všemohúci Bože,

ty si poslal na svet svojho Syna ako pravé svetlo; *
prosíme ťa za všetkých ľudí,

nech Duch Svätý otvára ich srdcia pravde

a vzbudzuje v nich vieru, -

aby sa krstom zrodili k novému životu

a stali sa členmi tvojho ľudu.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

ZA OHLASOVANIE EVANJELIA 875

