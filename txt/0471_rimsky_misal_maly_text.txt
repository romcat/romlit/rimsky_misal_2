Lebo on zvíťazil nad hriechom a smrťou,
obnovuje celý vesmír
a v ňom znovu nadobúdame plnosť života.

Preto vykúpené ľudstvo po celom svete
raduje sa z Kristovho vzkriesenia

a v nebi anjeli so zástupmi svätých
neprestajne spievajú na tvoju slávu:

Svätý, svätý, svätý Pán Boh všetkých svetov...

VEĽKONOČNÁ v

0 Kristovi, ktorý je kňazom i obeťou

Táto pieseň vďaky sa používa vo veľkonočnom období.

: Pán s vami.

; I s duchom tvojím.

: Hore srdcia.

; Máme ich u Pána.

; Vzdávajrne vďaky Pánovi, Bohu nášmu.
; Je to dôstojné a správne.

QŠPŠQŠ

]e naozaj dôstojné a správne, dobré a spásonosné
teba, Pane, velebit' v každom čase,

ale slávnostnejšie v tento čas,

keď sa Kristus obetoval

ako náš veľkonočný Baránok.

Lebo keď na kríži obetoval svoje telo,

zavŕšil obety Starého zákona.

A keď vydal sám seba za našu spásu,

bol zároveň kňazom, oltárom i obetným Baránkom.

VEĽKONOČNÁ V 371

