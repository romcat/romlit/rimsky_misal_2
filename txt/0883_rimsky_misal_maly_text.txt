A

ÚVODNY SPEV (Ez 36, 25-26)

Pán hovorí: Vylejem na vás vodu čistú,
dám vám srdce nové
a nového ducha vložím do vás.

MODLITBA DNA

Všemohúci a milosrdný Bože, *

prosíme t'a, zošli nám Ducha Svätého, —

aby v nás prebýval

a urobil z nás dôstojný chrám svojej slávy.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

Alebo :

Prosíme ťa, Bože,

láskavo splň na nás, čo si prisľúbil, *

že príde Duch Svätý —

a spôsobí, že pred svetom budeme svedčiť

o evanjeliu nášho Pána Ježiša Krista,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

Možno voliť aj iné modlitby (pozri str. 786 a 787).

NAD OBETNYMI DARMI

Bože, tvoj Syn nám vykúpením
získal Ducha Svätého; *
prijmi obetné dary svojich služobníkov a služobníc,

PRI UDEĽOVANÍ BIRMOVKY 783

