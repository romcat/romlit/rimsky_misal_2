NAD OBETNYMI DARMI

Prosíme ťa, všemohúci Bože, *

posvät' tieto obetné dary ——

a očistí naše srdcia ohňom Ducha Svätého.
Skrze Krista, nášho Pána.

PIESEN VĎAKY

Pán zoslal Ducha Svätého Cirkvi

: Pán s vami.

: I s duchom tvojím.

: Hore srdcia.

: Máme ich u Pána.

: Vzdávajme vďaky Pánovi, Bohu nášmu.
: Je to dôstojné a správne.

PŠPŠPŠ

Je naozaj dôstojné a správne, dobré a spásonosné
vzdávať vďaky vždy a všade

tebe, Pane, svätý Otče, všemohúci a večný Bože,
skrze nášho Pána Ježiša Krista.

Lebo on vystúpil na nebesia,

zasadol po tvojej pravici

a zoslal prisľúbeného Ducha Svätého na deti,
ktoré si ty prijal za svoje.

Preto teraz i po celú večnosť

chceme ti so zástupmi anjelov vrúcne spievať
a nadšene ťa oslavovať:

Svätý, svätý, svätý Pán Boh všetkých svetov...

SPEV NA PRIJÍMANIE (Porov. Ž 67, 29)

Upevni, Bože, čo si pre nás vykonal,
zo svojho svätého chrámu.

936 VOTÍVNE OMŠE

