alebo nejakú osobitnú slávnosť,
kňaz môže alebo spojiť vynechané
časti s inými s prihliadnutím na
zaradenie čítaní celého týždňa, ale-
bo rozhodnúť, ktorej stati sa má
dať prednosť.

V omšiach pre nejaké osobitné
zhromaždenie kňaz môže zo schvá-
leného lekcionára vybrať tie číta-
nia, ktoré sú pre také zhromažde-
nie najvhodnejšie.

320. Okrem toho je osobitný výber
textov Svätého písma pre omše, pri
ktorých sa vysluhujú sviatosti ale-
bo sväteniny, alebo ktoré sa slávia
pri nejakých mimoriadnych príle-
žitostiach.

Tieto lekcionáre slúžia na to,
aby počúvanie primeraného Bo-
žieho slova viedlo veriacich k pln-
šiemu pochopeniu tajomstva, na
ktorom sa zúčastňujú, a aby ich
vychovávalo k väčšej láske k Bo-
žiemu slovu.

Teda texty, ktoré sa čítajú v li-
turgickom zhromaždení, treba ur-
čiť s prihliadnutím na pastoračnú
vhodnosť a na možnosť výberu,
ktorá sa v tejto veci dáva.

Modlitby

321. Väčší počet prefácií, ktorými
je obohatený Rímsky misál, má
ten cieľ, aby sa z rozličných hľa-
dísk vysvetlil dôvod na vzdávanie
vďaky v eucharistickej modlitbe
a aby sa v plnšom svetle pred-
stavili rozličné aspekty tajomstva

spásy.

322. Volba medzi eucharistickými
modlitbami sa riadi týmito smer-
nicami:

a) Prvú eucharistickú modlitbu
čiže Rímsky kánon možno po-
užívať vždy. Zvlášť vhodné je po—

uživať ju v dňoch, ktoré majú vlast-
né V spoločenstve alebo Prosíme
t'a, Bože, ako aj na sviatky apošto-
lov a svätých, o ktorých je zmienka
v tejto eucharistickej modlitbe;
v nedele, ak sa z pastoračných dô-
vodov nedá prednosť inej eucha-
ristickej modlitbe.

b) Druhá eucharistická modlitba
sa pre svoje osobitné vlastnosti ho-
dí na všedné dni týždňa alebo pri
osobitných príležitostiach.

Hoci má vlastnú pieseň vďaky
(prefáciu), možno ju použiť aj s i-
nými prefáciami, najmä s tými,
ktoré súhrnne vyjadrujú tajomstvo
spásy, napr. nedeľné alebo spoloč-
né prefácie.

Keď je omša za niektorého zo-
snulého, možno použiť osobitnú
formulu vsunutú pred Pamätaj.

c) Tretia eucharistická modlitba
sa môže spájať s akoukoľvek pies-
ňou vďaky (prefáciou). Nech sa
uprednostní najmä v nedele a
sviatky.

V tejto eucharistickej modlitbe
možno použiť osobitnú formulu za
zosnulého, uvedenú na svojom
mieste po slovách a láskavo priveď
k sebe, dobrotivý Otče, všetky
svoje roztratené deti.

d) Štvrtá eucharistická modlitba
má nezameniteľnú pieseň vďaky
(prefáciu) a obsažnejšie podáva
dejiny spásy. Môže sa upotrebiť,
ked omša nemá vlastnú pieseň
vďaky.

Do tejto eucharistickej modlitby
pre jej štruktúru nemožno vsúvat
osobitnú modlitbu za zomrelého.

e) Eucharistická modlitba, ktorá
má vlastnú prefáciu, môže sa pou-
žiť spolu s ňou aj vtedy, keď sa
má v omši brať prefácia z niekto-
rého liturgického obdobia.

VŠEOBECNÉ SMERNICE - MlSÁL 69*

