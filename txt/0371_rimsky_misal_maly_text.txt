NAD OBETNYMI DARMI

Prosíme ťa, všemohúci Bože,

nech príde Duch Svätý *

a pripraví našu myseľ na eucharistická obe—tu, —-
lebo v ňom máme odpustenie všetkých hriechov.
Skrze Krista, nášho Pána.

Pieseň vďaky veľkonočná (str. 367 n.) alebo o Nanebovstúpení (str. 372 n.).

SPEV NA PRIJÍMANIE (11; 16, 14)

Pán Ježiš hovori: Duch Svätý ma oslávi,
lebo vezme z môjho a zvestuje vám to. Aleluia.

PO PRIJÍMANÍ

Milosrdný Bože,

ty si viedol svoj ľud od starozákonných obradov
k novozákonným sviatostiam; *

vrúcne t'a prosíme, — .

pomáhaj nám odložit starý spôsob života

a obnoviť si srdce láskou Ducha Svätého.

Skrze Krista, nášho Pána.

SOBOTA PO SIEDME] NEDELI 271

