NAD OBETNYMI DARMI

Láskavý Bože,

prinášame ti obetné dary

pri spomienke na svätú Máriu Magdalénu; *
láskavo ich prijmi, —

ako tvoj Syn dobrotivo prijal jej službu,
ktorú mu z lásky preukázala.

O to ten prosíme skrze Krista, nášho Pána.

SPEV NA PRIJÍMANIE (2 Kor 5, 14-15)

Kristova láska nás roznecuje, aby aj tí, čo žijú, už nežili pre seba,
ale pre toho, ktorý za nich zomrel a vstal z mŕtvych.

PO PRIJÍMANÍ

Dobrotivý Bože, *

nech nás nábožné prijatie tejto sviatosti
naplní takou vrúcnou a vytrvalou láskou, —
s akou sa svätá Mária Magdaléna

ustavične pridŕžala svojho Učiteľa a Pána,
tvojho Syna Ježiša Krista, ktorý s tebou žije
a kraľuje na veky vekov.

23. júla
SV. BRIGITY, REHOĽNÍČKY

Spoločná omša svätých žien (str. 766 n.).

MODLITBA DNA

Pane a Bože náš,
ty si svätej Brigite zjavil nebeské tajomstvá,
keď rozjímala o umučení tvojho Syna; *

23. ]ÚLA 575

