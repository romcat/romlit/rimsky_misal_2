NAD OBETNYMI DARMI

Dobrotivý Otče,

pozri na lásku svojho Syna,

ktorý sa obetoval za vykúpenie všetkých ľudí; *
daj, prosíme, —

aby sa skrze neho

zvelebovalo tvoje meno medzi národmi

od východu slnka až po jeho západ

a aby sa ti všade prinášala čistá obeta.

Skrze Krista, nášho Pána.

SPEV NA PRIJÍMANIE (Porov. Mt 28, za)

Pán Ježiš hovorí:
Učte všetky národy zachovávať všetko, čo som vám prikázal.
]a som s vami po všetky dni až do skončenia sveta.

PO PRIJÍMANÍ

Láskavý Bože,

posilnil si nás sviatosťou nášho vykúpenia; *
prosíme ťa, —

daj, aby sa jej pôsobením

vždy a všade šírila pravá viera.

Skrze Krista, nášho Pána.

ÚVODNY' SPEV (Ž 95, H)

Zvestujte Pánovu slávu pohanom
a jeho zázraky všetkým národom.
Lebo veľký je Pán a všetkej chvály hoden.

876 OMŠE ZA ROZLIČNÉ POTREBY

