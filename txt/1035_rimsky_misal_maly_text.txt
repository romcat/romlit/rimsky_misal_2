Kto verí vo mňa, o tom platí, čo hovorí Písmo:
Z jeho vnútra potečů prúdy živej vody.

Alebo : (Jn 19, 54)

Jeden z vojakov kopijou prebodol ježišovi bok,
a hneď vyšla krv a voda.

PO PRIJÍMANÍ

Milostivý Otče, vo svojej nevýslovnej dobrote
dal si nám účasť na sviatosti tvojej lásky; *
dopraj, prosíme, ——

aby sme sa na zemi stali podobnými Kristovi
a v nebi mali podiel na jeho sláve.

Skrze Krista, nášho Pána.

7. o DUCHU SVÁTOM

Táto omša sa slávi v červenom rúchu.

A

ÚVODNY SPEV (Porov. Rim 5, 5; 8, n)

Božia láska sa rozlieva v našich srdciach
skrze Ducha Svätého, ktorý v nás prebýva.

MODLITBA DNA

Večný Bože,

ty osvecuješ srdcia veriacich

svetlom Ducha Svätého; *

daj, prosíme, -—

aby sme v tomto Duchu poznávali, čo je správne,
a vždy sa radovali z jeho útechy a posily.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

O DUCHU SVÁTOM 935

