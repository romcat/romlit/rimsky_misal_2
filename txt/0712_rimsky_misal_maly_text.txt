NAD OBETNYMI DARMI

Dobrotivý Bože,

prinášame ti obetné dary

pri spomienke na košických mučeníkov; *
posilňuj i nás vo viere, —

aby sme vo všetkých protivenstvách
neohrozene vyznávali tvoje meno.

Skrze Krista, nášho Pána.

SPEV NA PRIJÍMANIE (Mt 10, 32)

Pán Ježiš hovorí: Každého, kto ma vyzná pred ľudmi,
aj ja vyznám pred svojím Otcom,
ktorý je na nebesiach.

PO PRIJÍMANÍ

Všemohúci Bože,

posilnil si nás predrahým telom a krvou

svojho jednorodeného Syna; *

obdaruj nás milosťou, —

aby sme sa podľa príkladu košických mučeníkov
verne pridŕžali Ježiša Krista a jeho Cirkvi.

Lebo on žije a kraľuje na veky vekov.

8. septembra
NARODENIE PANNY MÁRIE

Sviatok

ÚVODNY SPEV

S radosťou slávime narodenie
Preblahoslavenej Panny Márie,
lebo z nej vzišlo Slnko spravodlivosti, Kristus, náš Boh.

Oslavná pieseň.

612 VLASTNÉ OMŠE SVÁTYCH

