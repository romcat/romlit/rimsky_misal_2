NAD OBETNYMI DARMI

Všemohúci Bože, *

prijmi naše pokorné prosby a obetné dary
za tvojich verných služobníkov
prenasledovaných pre vieru, —

aby sa tešili,

že sú pridružení k obete tvojho Syna,

a mali dôveru,

že ich mená sú zapísané v knihe života.
Skrze Krista, nášho Pána.

SPEV NA PRIJÍMANIE (Mt ;, 11—12)

Pán Ježiš hovorí: Blahoslavení ste,
keď vás budú pre mňa potupovať a prenasledovať.
Radujte sa a jasajte, lebo máte hojnú odmenu v nebi.

Alebo: (Mt IO, 32)

Pán Ježiš hovorí: Každého, kto mňa vyzná pred ľuďmi,
aj ja vyznám pred svojím Otcom, ktorý je na nebesiach.

PO PRIJÍMANÍ

Pane a Bože náš,

účinkom tejto sviatosti

utvrdzuj nás vo vernosti pravde *

a posilňuj našich prenasledovaných bratov a sestry, —
aby vytrvalo niesli svoj kríž po stopách tvojho Syna
a vo všetkých protivenstvách

smelo vyznávali svoju vieru.

Skrze Krista, nášho Pána.

ZA PRENASLEDOVANYCH KRES'Í'ANOV 879

