a z otroctva hriechu,
vracia im Božiu milosť
a vovádza ich do spoločenstva svätých.

Toto je noc,
v ktorej Kristus rozlámal okovy smrti
a víťazne vstal z hrobu.

Aká nesmierna je voči nám tvoja dobrota, Otče,
aká nevyspytatelná je tvoja láska!

Aby si vykúpil otroka,

vydal si na smrť vlastného Syna!

Naozaj potrebný bol hriech Adamov,

ktorý zotrela smrť Kristova!

Č), šťastná vina,

pre ktorú k nám prišiel taký vznešený Vykupiteľ !
Posvätné tajomstvo tejto noci premáha zlobu,
zmýva viny,

hriešnikom vracia nevinnosť

a radosť zarmúteným.

Naozaj požehnaná noc,
ktorá spája nebo so zemou
a človeka s Bohom.

Preto v túto milostivú noc

prijmi, presvätý Otče,

našu večernú obetu chvály,

ktorú ti prináša svätá Cirkev,

keď rukami svojich služobníkov

slávnostne ti predkladá túto veľkonočnú sviecu,
pripravenú z vosku pracovitých včiel.

Preto ťa, Otče, prosíme,

nech táto svieca, zasvätená oslave tvojho mena,

VEĽKONOČNÁ VIGÍLIA 185

