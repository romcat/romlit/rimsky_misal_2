NAD OBETNYMI DARMI

Láskavý Bože, *

obeta, ktorú ti prinášame,

nech nás vnútorne očist'uje _

a nech nás zo dňa na deň lepšie pripravuje na život,
ktorý nás čaká v nebi.

Skrze Krista, nášho Pána.

Pieseň vďaky nedeľná (str. 374 n.).

SPEV NA PRIJÍMANIE (Porov- Ž 33. 9)

Skúste a presvedčte sa, aký dobrý je Pán;
šťastný človek, čo sa utieka k nemu.

Alebo: (Mt 11, 28)
Pán Ježiš hovorí:

Poďte ku mne všetci, ktorí sa namáhate a ste unavení,

a ja vás posilním.

PO PRIJÍMANÍ

Dobrotivý a večný Bože,

zahrnul si nás

nesmiernymi darmi svojej lásky; *
daj, aby sme Oltárnu sviatosť vždy
prijímali na naše spasenie —

a neprestajne ti za ňu ďakovali.
Skrze Krista, nášho Pána.

 

PÁTNÁSTA NEDEĽA

 

ÚVODNY SPEV (Ž 16, 15)

Ja však, ako je spravodlivé, uzriem tvoju tvár
a až raz vstanem zo sna, nasýtim sa pohľadom na teba.

Oslavná pieseň.

PÁTNÁSTA NEDEĽA 299

