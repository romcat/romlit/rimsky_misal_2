preukáž nám svoje milosrdenstvo

a vyslyš naše pokorné prosby, —

aby nás prejavy tvojej lásky

stále upevňovali vo viere v teba.

Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Nebeský Otče,

priniesli sme obetné dary na tvoj oltár *
a vrúcne t'a prosíme, —

posväť ich ohňom Ducha Svätého,

ktorý zostúpil na apoštolov

a rozplamenil ich srdcia.

Skrze Krista, nášho Pána.

Pieseň vďaky o Duchu Svätom I. (str. 936) alebo II. (str. 938).

SPEV NA PRIJÍMANIE (Porov. Ž 103, 30)

Pane, zošli svojho Ducha, a všetko ožije,
a obnovíš tvárnosť zeme.

PO PRIJÍMANÍ

Dobrotivý Bože, *

vznešená sviatosť, ktorú sme prijali,
nech v nás ustavične udržiava

oheň Ducha Svätého, -—

ktorého si zoslal na apoštolov.
Skrze Krista, nášho Pána.

940 VOTÍVNE OMŠE

