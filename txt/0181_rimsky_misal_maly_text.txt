a nech nám dá silu,
aby sme sa ochotnejšie premáhali.
Skrze Krista, nášho Pána.

Pieseň vďaky pôstna (str. 360 n.).

SPEV NA PRIJÍMANIE (Porov. z 24, 4)

Ukáž nám, Pane, svoje cesty
a pouč nás o svojich chodníkoch.

PO PRIJÍMANÍ

Prosíme t'a, všemohúci Bože, *

nech nás účasť na sviatostnej hostine
očistí od všetkých previnení, —

aby náš život zodpovedal

výZVam tvojej lásky.

Skrze Krista, nášho Pána.

 

SOBOTA
po Popolcovej strede

 

ÚVODNY' SPEV (Porov. Ž 68, 17)

Vyslyš nás, Pane,
ved si dobrotivý a láskavý,
pre svoje vel'ké milosrdenstvo pohliadni na nás.

MODLITBA DNA

Všemohúci a večný Bože,

milosrdne zhliadni na našu krehkosť *

a vystri nad nás svoju ochrannú ruku, —

aby sme zvíťazili v boji proti zlu.

O to ťa prosíme skrze nášho Pána Ježiša Krista,
tvojho Syna, ktorý je Boh a s tebou žije a kraľuje
v jednote s Duchom Svätým po všetky veky vekov.

SOBOTA PO POPOLCOVE] STREDE 81

