ktorými sme sa zriekli zlého ducha a jeho skutkov
a sľúbili sme,
že budeme slúžiť Bohu vo svätej katolíckej Cirkvi.

Preto sa vás pýtam:

Kňaz: Zriekate sa zlého ducha?
Všetci: Zriekam.

Kňaz: Aj všetkých jeho skutkov?
Všetci: Zriekarn.

Kňaz: Aj všetkých jeho pokušení?
Všetci: Zriekam.

Alebo:

Kňaz: Zriekate sa hriechu, aby ste mohli žiť v slo-
bode Božích detí?
Všetci: Zriekam.

Kňaz: Zriekate sa vábivých pokušení, aby Vás ne-
ovládal hriech?
Všetci: Zriekam.

Kňaz: Zriekate sa zlého ducha, pôvodcu a kniežaťa
hriechu?

Všetci: Zriekam.

Biskupská konferencia môže túto druhú formulu upravit podl'a miestnych
potrieb.

Kňaz pokračuje:

Kňaz: Veríte v Boha, Otca všemohúceho, Stvoriteľa
neba i zeme?

Všetci: Verím.

Kňaz: Veríte v Ježiša Krista, jeho jediného Syna a
nášho Pána, narodeného z Márie Panny, umučeného

202 VEĽKONOČNÁ VIGÍLIA

