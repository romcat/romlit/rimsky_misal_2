x5]. Ak nie je prítomný diakon,
môže po výzve kňaza na modlitbu
veriacich predniesť úmysly.

152. Ak sa úvodný spev a spev na
prijímanie nespieva a ani veriaci
nerecitujú text príslušných spevov
z misála, prednesie ich vo vhod-
nom čase sám lektor.

II.Koncelebrované
omše

Úvodné poznámky

15 3 _ Koncelebrácia, ktorou sa vhod-
ne prejavuje jednota kňazstva a
obety, ako aj všetkého Božieho
ľudu, je dovolená aj okrem prí-
padov, keď ju nariaďuje sám obrad,
a to:

i. a) Vo štvrtok Veľkého týždňa
tak v omši krizmy, ako aj
pri večernej omši;

b) v omšiach na konciloch,
biskupských schôdzkach a
synodách;

c) v omši žehnania opáta.

2. Okrem toho 5 povolením or-
dinára, ktorému prislúcha posúdiť
vhodnosť koncelebrácie, povoľuje
sa:

a) v konventuálnej omši, ako i
v hlavnej omši v kostoloch a orató-
riách, ak záujem veriacich nevy-
žaduje, aby všetci prítomní kňazi
slúžili omšu osobitne;

b) v omšiach z príležitosti
akýchkoľvek zhromaždení kňazov,
diecéznych alebo rehoľných?2

154. Kde je veľký počet kňazov,
zodpovedný predstavený môže po-
volit', aby sa koncelebrovalo aj
viackrát cez deň, no postupne ale-
bo na rozličných posvätných mies-
tach.63

155. Biskupovi prislúcha právo po-
dľa platných noriem usmerňovať
vo svojej diecéze koncelebrovanie
omší, a to aj v kostoloch a polo-
verejných oratóriách rehoľníkov
vyňatých z jeho právomoci. Právo
rozhodovať o vhodnosti konceleb-
rácie a povoľovat' ju vo svojich
kostoloch a oratóriách má každý
ordinár a vyšší predstavený kňaz-
ských reholí nevyňatých, ako aj
združení kňazov, žijúcich spoločne
bez sľubov.64

156. Nikoho neslobodno pripustiť
ku koncelebrácii, keď sa už omša
začala.“5

157. Osobitne si treba vážiť kon-
celebráciu, pri ktorej diecézni kňa-
zi koncelebrujú so svojím bisku—
pom, najmä pri omši so svätením
olejov vo štvrtok Veľkého týždňa a
z príležitosti synody alebo pasto-
račnej vizitácie. Preto sa konceleb-
rácia odporúča vždy, keď sa kňazi
stretnú so svojím biskupom na
duchovných cvičeniach alebo na
nejakých schôdzach. V takýchto
prípadoch sa ešte zreteľnejšie pre-
javuje onen znak jednoty kňazstva
a Cirkvi, ktorý sa zračí v každej
koncelebrácii.66

158. Povolenie viackrát sláviť svä-
tú omšu alebo koncelebrovať v ten

“ Porov. Druhý valik. koncil, konšt. o posv. liturgii Sarromnrtum Concíĺx'um, č. 57.
5“ Porov. Posv. kongr. obradov, ínštr. Eutharirtimm m_yneríum, 25. mája 1967, č. 47: AAS 59 (1967)

str. 566.

“ Porov. Rítus servandus in concelebratione Missae, č. 3.

“ Porov. tamže, &. 8.

“ Porov. Posv. kongr. obradov, všeob. dekrét Ertlen'ae semper, 7. marca 1965: AA.? 57 (1965) str.
410—412; inšrr. Eucharirtimm m_ynerium, 15. mája X967, č. 47: AAS 59 (1967) str. 565.

VŠEOBECNÉ SMERNICE - MISÁL 47*

