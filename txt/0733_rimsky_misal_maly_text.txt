PO PRIJÍMANÍ

Milosrdný Bože, *

nech toto sväté prijímanie

zapáli v nás oheň lásky, —

aby sme sa ti úplne oddali ako svätá Terézia

a podľa jej príkladu vyprosovali všetkým ľuďom
tvoje zľutovanie.

Skrze Krista, nášho Pána.

2. októbra
SVÁTYCH ANJELOV STRÁŽCOV
Spomienka
ÚVODNý SPEV (Dan 3, 58)

Všetci Boží anjeli, dobrorečte Pánovi,
chválte ho a oslavujte naveky.

MODLITBA DNA

Láskavý Bože, vo svojej prozretel'nosti

posielaš nám svätých anjelov za strážcov; *
vrúcne ťa prosíme, —

daj, aby sme vždy cítili ich mocnú ochranu

a raz v ich spoločenstve prežívali večnú radosť.
Skrze nášho Pána Ježiša Krista, tvojho Syna,

ktorý je Boh a s tebou žije a kraľuje

v jednote s Duchom Svätým po všetky veky vekov.

NAD OBETNYMI DARMI

Všemohúci Bože, *

prijmi obetné dary, ktoré ti prinášame

na počesť tvojich svätých anjelov, —

aby nás chránili pred nebezpečenstvami tohto sveta

2. OKTĎBRA 633

