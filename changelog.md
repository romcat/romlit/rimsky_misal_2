## Change Log

| **Date**          | **Changes** |
|-------------------|-------------|
| 01 December 2019  | Add missing PNG/TXT files (the first 100 pages of `rimsky_misal_maly_text`) | Remove version numbers from `changelog.md` <br> Add PNG and initial TXT files of `rimsky_misal_maly_text.pdf` <br> Update a few files |
| 01 December 2019  | Add missing PNG/TXT files (the first 100 pages of `rimsky_misal_maly_text`) | Remove version numbers from `changelog.md` |
| 20 October 2019   | Add PNG and initial TXT files of `rimsky_misal_maly_text.pdf` <br> Update a few files |
| 15 September 2019 | Initial version |