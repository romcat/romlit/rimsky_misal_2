## Rímsky misál, 2. vydanie

Projekt na spracovanie slovenského _Rímskeho misálu_, teda prekladu _Missale romanum_, editio typica altera.

Mojím cieľom je nascannovaný _Rímsky misál_ OCR-knúť, skontrolovať správnosť textu a prepísať v XeTeXu s použitím Lylipondu pre modernú notáciu a Gabc pre gregoriánsku notáciu.

## Spôsob elektronifikácie

V základe som použil programy `pdfimages` na konverziu PDF-iek do PNG-cok a potom `tesseract` na samotné OCR-knutie obrázkov na texty. Následne manuálne urobil korekciu textu a pridal LaTeXovské príkazy (na začiatok len základné ako tučné písmo, kurzíva, podčiarknutie, ale aj poznámky pod čiarou), neskôr mám v pláne celé to naformátovať v XeLaTeXu vo formáte veľkosti A4.

Konkrétne som si vytvoril funkciu [`pdfocr`](https://gitlab.com/tukusejssirs/lnx_scripts/blob/master/bash_functions/pdfocr.sh), ktorú som spustil takto: `pdfocr [nazov].pdf rimsky_misal_maly_uvod slk - - -`. Samozrejme, tá funkcia nie je dokonalá a zišli by sa v nej použiť `getopts`.

### Licencia

Práva nad textom vlastní Spolok svätého Vojtecha a tu je použitý bez pridelenej licencie.

[Licencia](licence.txt) je platná na spracovanie, nie na samotný text.